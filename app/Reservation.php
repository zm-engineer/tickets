<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Reservation extends Model
{
    use SoftDeletes;

    public function ticket()
    {
        return $this->hasOne('App\Ticket', 'id', 'ticket_id');
    }

    public function evento()
    {
        return $this->hasOne('App\Evento', 'id', 'evento_id');
    }

    public function usuario()
    {
        return $this->hasOne('App\User', 'id', 'usuario_id');
    }
}
