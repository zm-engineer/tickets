<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Evento extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'id', 
    ];

    public function categorias() {
        return $this->belongsToMany('App\Categoria');
    }

    public function reservation()
    {
        return $this->belongsTo('App\Reservation');
    }

    public function mapa()
    {
        return $this->belongsTo('App\Mapa');
    }

    public function tipo()
    {
        return $this->belongsTo('App\Tipo');
    }

    public function preguntas() {
        return $this->hasMany(Pregunta::class);
    }

    public function eventosCategorias() {
        return $this->hasMany(EventoCategoria::class);
    }
}
