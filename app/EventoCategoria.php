<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventoCategoria extends Model
{
   use SoftDeletes;
   protected $table = 'categoria_evento';

    public function evento(){
        return $this->belongsTo(Evento::class, 'evento_id');
    }

    public function categoria(){
        return $this->belongsTo(Categoria::class, 'categoria_id');
    }
}
