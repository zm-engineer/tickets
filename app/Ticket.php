<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use SoftDeletes;

    public function grupo(){
        return $this->hasOne(Grupo::class, 'id', 'grupo_id');
    }

    public function reservation()
    {
        return $this->belongsTo('App\Reservation');
    }

    public function nota()
    {
        
        return $this->belongsTo(Nota::class);
    }
}
