<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categoria extends Model
{
    use SoftDeletes;
    
    public function eventos() {
        return $this->belongsToMany('App\Evento')
        ->whereNull('categoria_evento.deleted_at')
        ->withTimestamps();
    }
}
