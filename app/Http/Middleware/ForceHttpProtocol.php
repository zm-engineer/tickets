<?php

namespace App\Http\Middleware;

use Closure;

class ForceHttpProtocol {

    public function handle($request, Closure $next) {

        if (env('APP_ENV') === 'pro') {

          if (substr($request->header('Host'), 0, 4)  !== 'www.') {
               $request->headers->set('Host', 'www.reservadetickets.es');
               return redirect()->secure($request->getRequestUri());
          }

          if (!$request->secure()) {
              return redirect()->secure($request->getRequestUri());
          }

        }

        return $next($request);
    }

}