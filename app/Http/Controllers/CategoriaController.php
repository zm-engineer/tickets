<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Evento;
use App\EventoCategoria;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::with('eventos')->get();
        $eventos = Evento::all();

        return [$categorias, $eventos];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->all());

        $categoria = new Categoria;

        $categoria->nombre = $request->nombre;
        $categoria->color = $request->color;
  
        $categoria->save();

         //eventos categoria
         foreach ($request->eventosSeleccionados as $key => $evento) {

            $nuevoEventoCategoria = new EventoCategoria;

            $nuevoEventoCategoria->categoria_id = $categoria->id;
            $nuevoEventoCategoria->evento_id = $evento['id'];

            $nuevoEventoCategoria->save();
            
        }
  
        return response()->json('Registrado correctamente.', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoria = Categoria::find($id);

        $categoria->nombre = $request->nombre;
        $categoria->color = $request->color;
  
        $categoria->save();

        //eventos categoria
        foreach ($request->eventosSeleccionados as $key => $evento) {
            // dd($request->eventosSeleccionados, $evento['id']);
            $eventoCategoria = EventoCategoria::where('categoria_id', $id)->where('evento_id', $evento['id'])->first();

            if(!$eventoCategoria)
            {
                $nuevoEventoCategoria = new EventoCategoria;

                $nuevoEventoCategoria->categoria_id = $id;
                $nuevoEventoCategoria->evento_id = $evento['id'];

                $nuevoEventoCategoria->save();
            }

            $eventoCategorias = Categoria::where('id', $id)->with('eventos')->first()->toArray();
         
        }


        $diferentes = array_udiff($eventoCategorias['eventos'], $request->eventosSeleccionados,
            function ($obj_a, $obj_b) {
                return $obj_a['id'] - $obj_b['id'];
            }
        );


        // dd($eventoCategorias['eventos'], $request->eventosSeleccionados, $diferentes);

        //recorrer los diferentes obtenidos y eliminarlos
        foreach ($diferentes as $key => $diferente) {
            // dd($diferente['pivot']['categoria_id'], $diferente['pivot']['evento_id']);
            $eventoCategoria = EventoCategoria::where('categoria_id', $diferente['pivot']['categoria_id'])->where('evento_id', $diferente['pivot']['evento_id'])->first();
            EventoCategoria::destroy($eventoCategoria->id);
        }
  
        $categorias = Categoria::with('eventos')->get();

        return [$categorias];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $eventosCategorias = EventoCategoria::where('categoria_id', $id)->get();

        foreach ($eventosCategorias as $key => $eventoCategoria) {
            EventoCategoria::destroy($eventoCategoria->id);
        }
        Categoria::destroy($id);

        return response()->json('Categoria eliminada correctamente.', 200);
    }
}
