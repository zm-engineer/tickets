<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\User;
use Mail;
use App\Helpers\Frontend\EnviosCorreo as HelperCorreo;
use Browser;

class AuthController extends Controller
{
  public function login(Request $request)
  {

    try {
      $user = User::where('email', $request->username)->first();
      // dd($user);
    
      if($user && $user->navegador_unico)
      {
       
        if(!$user->online)
        {
          // $this->iniciarSesion($user);
          $request->request->add([
            'grant_type' => 'password',
            'client_id' => 2,
            'client_secret' => '7WJVCsf1uWr3aghhpvqfn13TiBjJQeHobQChlFcM',
            'scope' => '*'
          ]);
      
          $tokenRequest = Request::create(
            'http://127.0.0.1:8000/oauth/token',
              'post'
          );

      
          $response = Route::dispatch($tokenRequest);

          // dd($response);test

          if($response->getStatusCode() == 200)
          {
      
            $user->online = 1;
            $user->ocultar_precios = $user->ocultar_precios + 1; //cantidad de dispositivo conectados
            $user->save();
      
            return $response->getContent();
          }
          else if ($response->getStatusCode() === 400)
          {
            return response()->json('Tus credenciales son incorrectas, o su contraseña ha vencido, sugerimos recuperar contraseña.', 400);
          }
          else if ( $response->getStatusCode() === 401)
          {
            return response()->json('No está autorizado, comuniquese con el administrador.', 401);
          }
      
          return response()->json('Algo salió mal en el servidor.', $response->getStatusCode());
      
        }
        else
        {
          return response()->json('Se ha encontrado una sesión abierta, desde otro navegador o dispositivo.', 401);
        }

      }
      else
      {
        // $this->iniciarSesion($user);
        $request->request->add([
          'grant_type' => 'password',
          'client_id' => 2,
          'client_secret' => '7WJVCsf1uWr3aghhpvqfn13TiBjJQeHobQChlFcM',
          'scope' => '*'
        ]);
    
        $tokenRequest = Request::create(
          'http://127.0.0.1:8000/oauth/token',
            'post'
        );
    
        $response = Route::dispatch($tokenRequest);
    
        if($response->getStatusCode() == 200)
        {
    
          $user->online = 1;
          $user->ocultar_precios = $user->ocultar_precios + 1;
          $user->save();
    
          return $response->getContent();
        }
        else if ($response->getStatusCode() === 400)
        {
          return response()->json('Tus credenciales son incorrectas, o su contraseña ha vencido, sugerimos recuperar contraseña.', 400);
        }
        else if ( $response->getStatusCode() === 401)
        {
          return response()->json('No está autorizado, comuniquese con el administrador.', 401);
        }
    
        return response()->json('Algo salió mal en el servidor.', $response->getStatusCode());
    
      }  


    } catch (\Throwable $th) {
      return response()->json('Hubo un error al tratar de conectarse con el servidor, intentelo mas tarde.', 400);

    }

  }

  public function iniciarSesion($user){

    $request->request->add([
      'username' => $request->username,
      'password' => $request->password,
      'grant_type' => 'password',
      'client_id' => 2,
      'client_secret' => '7WJVCsf1uWr3aghhpvqfn13TiBjJQeHobQChlFcM',
      'scope' => '*'
    ]);

    $tokenRequest = Request::create(
      'http://127.0.0.1:8000/oauth/token',
        'post'
    );

    $response = Route::dispatch($tokenRequest);

    if($response->getStatusCode() == 200)
    {

      $user->online = 1;
      $user->save();

      return $response->getContent();
    }
    else if ($response->getStatusCode() === 400)
    {
      return response()->json('Tus credenciales son incorrectas, o su contraseña ha vencido, sugerimos recuperar contraseña.', 400);
    }
    else if ( $response->getStatusCode() === 401)
    {
      return response()->json('Tus credenciales son incorrectas. Inténtalo de nuevo', 401);
    }

    return response()->json('Algo salió mal en el servidor.', $response->getStatusCode());

  }

  public function register(Request $request)
  {
      $request->validate([
        'nombre' => 'required',
        'username' => 'required',
        'password' => 'required',
      ]);

      $user = new User;

      $user->name = $request->nombre;
      $user->email = $request->username;
      $user->password = bcrypt($request->password);

      $user->save();

      return response()->json('Registrado correctamente.', 200);

  }

  public function logout()
  {

      $user = auth()->user();
      // dd($user);

      //ocuktar precios es la cantidad de dispositivo iniciado sesion
      if($user->ocultar_precios == 1){
        $user->online = 0;
        $user->ocultar_precios = 0;

        auth()->user()->tokens->each(function ($token, $key) {
          $token->delete();
        });
      }else{
        $user->ocultar_precios = $user->ocultar_precios - 1;
      }
      $user->save();

      return response()->json('Cerró sesión correctamente', 200);
  }

  public function codigo(Request $request){

    $user = User::where('email', $request->username)->first();

    if($user){
      $correo = $request->username;
      $subject = 'Código '.$request->codigo;
      HelperCorreo::sendMail('emails.recuperacion', $request, $correo, $subject);

      return response()->json('Código enviado exitosamente.');
    }else{
      return response()->json('Usuario no existe.');
    }

  }

  public function recuperar(Request $request)
  {
    $user = User::where('email', $request->username)->first();

    if($user){
       $user->password = bcrypt($request->password);
       $user->save();

      return response()->json('Contraseña cambiada exitosamente.');
    }else{
      return response()->json('Usuario no existe.');
    }
  }
}
