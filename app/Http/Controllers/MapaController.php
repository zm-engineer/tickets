<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mapa;
class MapaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mapas = Mapa::all();
        return $mapas;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all(), !isset($request['id']));

        if(!isset($request['id'])){
            $ruta_imagenes = public_path('imagenes');

            if($request['imagen'] != "null"){
                $imagen = $request['imagen']->getClientOriginalName();
                $nombre_mapa = 'mapa_'.time() . '.' . $request['imagen']->getClientOriginalExtension();
                $request['imagen']->move($ruta_imagenes, $nombre_mapa);
            }
    
            $mapa = new Mapa;
    
            $mapa->estadio = $request['estadio'];
            if($request['imagen'] != "null")
                $mapa->imagen = $nombre_mapa;
    
            $mapa->save();
            return $mapa;

        }else{

            $ruta_imagenes = public_path('imagenes');

            if($request['imagen'] != "null"){
                $imagen = $request['imagen']->getClientOriginalName();
                $nombre_mapa = 'mapa_'.time() . '.' . $request['imagen']->getClientOriginalExtension();
                $request['imagen']->move($ruta_imagenes, $nombre_mapa);
            }
    
           
            $mapa = Mapa::find($request['id']);
           
            $mapa->estadio = $request->estadio;
                if($request['imagen'] != "null")
                 $mapa->imagen = $nombre_mapa;
      
            $mapa->save();
    
            $mapas = Mapa::all();
    
            return $mapas;
        }
        

    }
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Mapa::destroy($id);

        return response()->json('Mapa eliminado correctamente.', 200);
    }
}
