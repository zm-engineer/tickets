<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pregunta;
use App\Evento;

class PreguntaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $preguntas = Pregunta::with('evento')->get();
        $eventos = Evento::all();

        return [$preguntas, $eventos];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pregunta = new Pregunta;

        $pregunta->pregunta = $request->pregunta;
        $pregunta->respuesta = $request->respuesta;
        $pregunta->evento_id = $request->eventoId;
        $pregunta->home = $request->home;
  
        $pregunta->save();

        return response()->json('Registrado correctamente.', 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd(isset($request->pregunta));
        $pregunta = Pregunta::find($id);

        if(isset($request->pregunta['pregunta'])){
            $pregunta->pregunta = $request->pregunta['pregunta'];
            $pregunta->respuesta = $request->pregunta['respuesta'];
            $pregunta->evento_id = $request->pregunta['evento_id'];
            $pregunta->home = $request->pregunta['home'];

        }else{
            $pregunta->pregunta = $request->pregunta;
            $pregunta->respuesta = $request->respuesta;
            $pregunta->evento_id = $request->eventoId;
            $pregunta->home = $request->home;

        }
        
  
        $pregunta->save();

        $preguntas = Pregunta::with('evento')->get();

        return $preguntas;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pregunta::destroy($id);

        return response()->json('Pregunta eliminada correctamente.', 200);
    }
}
