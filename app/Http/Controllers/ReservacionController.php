<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;
use App\User;
use App\Ticket;
use App\Evento;
use App\Configuracion;
use Mail;
use App\Helpers\Frontend\EnviosCorreo as HelperCorreo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ReservacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        // dd($user);

        // dd(Carbon::now()->add(-7, 'day')->format('Y-m-d'));

        if($user->rol != 'admin')
        {
            $reservas = DB::table('reservations')
            ->Join('users','users.id','reservations.usuario_id')
            ->Join('eventos','eventos.id','reservations.evento_id')
            ->Join('tickets','tickets.id','reservations.ticket_id')
            ->Join('grupos','grupos.id','tickets.grupo_id')
            ->Join('categoria_evento','categoria_evento.id','grupos.categoria_evento_id')
            ->Join('categorias','categorias.id','categoria_evento.categoria_id')
            ->where('users.id', $user->id)
            ->where('reservations.deleted_at', null)
            ->whereDate('reservations.created_at', '>=', Carbon::now()->add(-7, 'day')->format('Y-m-d'))
            ->orderBy('created_at', 'Desc')
            ->select('eventos.nombre as evento', 'eventos.fecha as fecha', 'categorias.nombre as categoria', 'tickets.sector', 'tickets.juntas', 'tickets.formato', 'reservations.id', 'reservations.conserje', 'reservations.created_at', 'reservations.precioUd', 'reservations.plus', 'reservations.comision', 'reservations.numero')
            ->get();
        }
        else
        {
            $reservas = DB::table('reservations')
            ->Join('users','users.id','reservations.usuario_id')
            ->Join('eventos','eventos.id','reservations.evento_id')
            ->Join('tickets','tickets.id','reservations.ticket_id')
            ->Join('grupos','grupos.id','tickets.grupo_id')
            ->Join('categoria_evento','categoria_evento.id','grupos.categoria_evento_id')
            ->Join('categorias','categorias.id','categoria_evento.categoria_id')
            ->orderBy('created_at', 'Desc')
            ->where('reservations.deleted_at', null)
            ->select('eventos.nombre as evento', 'eventos.fecha as fecha', 'users.name as cliente', 'users.email as usuario', 'categorias.nombre as categoria', 'tickets.sector', 'tickets.juntas', 'tickets.formato', 'reservations.id', 'reservations.conserje', 'reservations.created_at', 'reservations.precioUd', 'reservations.plus', 'reservations.comision', 'reservations.numero' )
            ->get();
        }
       

        return $reservas;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reservacionEncontrada = Reservation::where('ticket_id', $request->ticket)->first();
        $ticketStock = Ticket::where('id', $request->ticket)->where('estado', 2)->first();
        // dd($reservacionEncontrada, $ticketStock );

        $user = Auth::user();

        if($reservacionEncontrada || $ticketStock && $user->rol != 'admin')
        {
            return 'Éste ticket ya ha sido reservado o no está disponible.';
        }
        else
        {
            $ticket = Ticket::where('id',$request->ticket)->with('grupo')->first();
            $usuario = User::find($request->usuario);

            $reservacion = new Reservation;
            $reservacion->ticket_id = $request->ticket;
            $reservacion->usuario_id = $request->usuario;
            $reservacion->evento_id = $request->evento;
            $reservacion->conserje = $request->conserje;
            $reservacion->precioUd = $ticket->precioUd;
            $reservacion->plus = $usuario->plus;
            $reservacion->comision = $usuario->comision;
            $reservacion->numero = $ticket->grupo->numero;
            $reservacion->save();

            //cambiar el ticket a estado de reservado
            $ticket->estado = 3;
            if($request->precioFinal)
                $ticket->precioFinal = $request->precioFinal;

            $ticket->save();

             //guardar el conserje en usuario
            if( $request->conserje)
            {
                $usuario = User::find($request->usuario);
                if($usuario->conserje){
                    $conserjesArreglo = explode(",", $usuario->conserje);
                    $existe = false;

                    foreach ($conserjesArreglo as $key => $value) {
                        if($value == $request->conserje)
                            $existe = true;
                    }

                    if(!$existe)
                        $usuario->conserje = $usuario->conserje.','.$request->conserje;
                }else{
                    $usuario->conserje = $request->conserje;
                }

                $usuario->save();
            }

            //enviar un correo al admin con la información de la reserva
            $reserva = Reservation::where('id', $reservacion->id)
            ->with('usuario')
            ->with('ticket')
            ->with('evento')
            ->first();

            $configuracion = Configuracion::first();

            $correo = $configuracion->email;

           // $correo = 'huguetebastida@gmail.com'; //cambiar por el correo del admin
            $subject = 'Han hecho una reservación.';
            HelperCorreo::sendMail('emails.reservacion', $reserva, $correo, $subject, $configuracion);
            // $reservas = Reservation::where('usuario_id', auth()->user()->id)->with('usuario')->get();
            $reservas = Reservation::with('usuario')->with('evento')->get();
                                        
            return $reservas;
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reserva = Reservation::find($id);
        $ticket = Ticket::find($reserva->ticket_id);

        Reservation::destroy($id);
        Ticket::destroy($ticket->id);

        $reservas = Reservation::with('usuario')->with('evento')->get();
                                        
        return $reservas;
    }
}
