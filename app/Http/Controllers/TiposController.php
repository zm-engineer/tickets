<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tipo;
class TiposController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos = Tipo::all();
        return $tipos;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all(), !isset($request['id']));

        if(!isset($request['id'])){
            $ruta_imagenes = public_path('imagenes');

            if($request['logo'] != "null"){
                $logo = $request['logo']->getClientOriginalName();
                $nombre_logo = 'tipo_'.time() . '.' . $request['logo']->getClientOriginalExtension();
                $request['logo']->move($ruta_imagenes, $nombre_logo);
            }
    
            $tipo = new Tipo;
    
            $tipo->nombre = $request['nombre'];
            if($request['logo'] != "null")
                $tipo->logo = $nombre_logo;
    
            $tipo->save();
            return $tipo;

        }else{

            $ruta_imagenes = public_path('imagenes');

            if($request['logo'] != "null"){
                $logo = $request['logo']->getClientOriginalName();
                $nombre_logo = 'tipo_'.time() . '.' . $request['logo']->getClientOriginalExtension();
                $request['logo']->move($ruta_imagenes, $nombre_logo);
            }
    
           
            $tipo = Tipo::find($request['id']);
           
            $tipo->nombre = $request->nombre;
            if($request['logo'] != "null")
            $tipo->logo = $nombre_logo;
      
            $tipo->save();
    
            $tipos = Tipo::all();
    
            return $tipos;
        }
        

    }
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tipo::destroy($id);

        return response()->json('tipo eliminado correctamente.', 200);
    }
}

