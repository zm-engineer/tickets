<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\Reservation;
use App\Grupo;
use App\Nota;

class TicketController extends Controller
{
    
    public function index()
    {
        $tickets = Ticket::with('grupo')->get();
        $reservas = Reservation::where('usuario_id', auth()->user()->id)->get();
        $reservasTodas = Reservation::with('usuario')->with('evento')->get();
        $grupos = Grupo::all();
        $notas = Nota::all();

        if($tickets)
        {
            return [$tickets, $reservas, $grupos, $reservasTodas, $notas];
        }
        else
        {
            return 'No encontrado';
        }
    }

   
    public function store(Request $request)
    {
        // dd($request->all(), $request->ticket['sector'], $request->ticket);
       
        $ticket = new Ticket;
        $ticket->sector = $request->ticket['sector'];
        $ticket->juntas = $request->ticket['juntas'];
        $ticket->formato = $request->ticket['formato'];
        $ticket->precioUd = floatval($request->ticket['precioUd']);
        $ticket->precioFinal = $request->ticket['precioFinal'];
        $ticket->grupo_id = $request->ticket['grupo_id'];
        $ticket->estado = $request->ticket['estado'];

        $ticket->save();

        $ticketCreado = Ticket::where('id', $ticket->id)->with('grupo')->first();

        $tickets = Ticket::with('grupo')->get();

        return [$tickets, $ticketCreado];

    }

   
    public function show($id)
    {
        //
    }

    public function prueba()
    {
        $h1 = '303ed4c69846ab36c2904d3ba8573050';
        $h2 = 'a7228a7d885a3f09046ee2a8fb2e3252';
        $h3 = '84eb13cfed01764d9c401219faa56d53';

        $whatsApp = [];

        foreach ([$h1, $h2, $h3] as $key => $value) {
            foreach (range(0,9) as $d1) {
                foreach (range(0,9) as $d2) {
                    foreach (range(0,9) as $d3) {
                        $digitos = strval($d1).strval($d2).strval($d3);
                        $cifrar = md5($digitos);

                        if($cifrar == $value)
                            array_push($whatsApp, $digitos);
                    }
                }
            }
        }


        dd($whatsApp);
    }

    
    public function actualizar(Request $request)
    {
        
        $ticket = Ticket::where('id', $request->ticketID['id'])->with('grupo')->first();
        $ticket->estado = $request->ticketEstado;

        $ticket->save();

        if($request->ticketID['estado'] == 3){
            //hacemos eliminado logico en reservaciones
            $reservationAeliminar = Reservation::where('ticket_id', $ticket->id)->first();
            Reservation::destroy($reservationAeliminar->id);
        }

        $tickets = Ticket::with('grupo')->get();


        return  [$ticket, $tickets];
    }

    public function nota(Request $request)
    {
        $ticket = Ticket::where('id', $request->ticketId)->first();

        if($ticket){
            $ticket->descripcion = $request->nota;
            $ticket->save();

        }


        $tickets = Ticket::with('grupo')->get();

        return $tickets;
    }

    public function cambiarTicketMax(Request $request){

        $grupo = Grupo::find($request->grupo['id']);
        $grupo->cantidad_max_disponible = $request->grupo['cantidad_max_disponible'];
        $grupo->permitir_ticket_max = $request->grupo['permitir_ticket_max'];

        $grupo->save();

        $tickets = Ticket::with('grupo')->get();

        return $tickets;

    }

    public function update(Request $request)
    {
        //  dd($request->all(), $request->ticket['grupo']['numero'], $request->ticket['grupo']);

        $ticket = Ticket::where('id', $request->ticket['id'])->with('grupo')->first();

        if($ticket->grupo->numero == $request->ticket['grupo']['numero'])
        {
            $ticket->grupo_id = $request->ticket['grupo_id'];
        }
        else
        {

            $grupo = Grupo::where('categoria_evento_id', $request->ticket['grupo']['categoria_evento_id'])
                        ->where('numero', $request->ticket['grupo']['numero'])->first();

            // dd($grupo);
            if(!$grupo)
            {
                $grupo = new Grupo;

                $grupo->numero = $request->ticket['grupo']['numero'];
                $grupo->cantidad_max_disponible = $request->ticket['grupo']['cantidad_max_disponible'];
                $grupo->permitir_ticket_max = $request->ticket['grupo']['permitir_ticket_max'];
                $grupo->categoria_evento_id = $request->ticket['grupo']['categoria_evento_id'];

                $grupo->save();

                $ticket->grupo->numero = $grupo->numero;
                $ticket->grupo_id = $grupo->id;

            }
            else
            {
                $ticket->grupo->numero = $grupo->numero;
                $ticket->grupo_id = $grupo->id;
            }

        }

        $ticket->sector = $request->ticket['sector'];
        $ticket->juntas = $request->ticket['juntas'];
        $ticket->formato = $request->ticket['formato'];
        $ticket->precioUd = floatval($request->ticket['precioUd']);
        $ticket->precioFinal = $request->ticket['precioFinal'];

        $ticket->save();

        $tickets = Ticket::with('grupo')->get();
        $grupos = Grupo::all();

        return [$tickets, $grupos];
    }

    public function destroy($id)
    {

        Ticket::destroy($id);

        $reserva = Reservation::where('ticket_id', $id)->first();
        if($reserva){
            Reservation::destroy($reserva->id);
        }

        $tickets = Ticket::with('grupo')->get();

        return $tickets;
    }
}
