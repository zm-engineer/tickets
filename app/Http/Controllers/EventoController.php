<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evento;
use App\Grupo;
use App\Ticket;
use App\User;
use App\EventoCategoria;
use App\Pregunta;
use App\Tipo;
use App\Mapa;
use Illuminate\Support\Facades\DB;


class EventoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventos = Evento::with('mapa')->with('tipo')->get();
        $hoy = date("Y-m-d H:i:s");
      
        $eventosDestacados = Evento::where('destacado', 1)->with('mapa')->with('tipo')->where('fecha', '>=', $hoy)->where('fecha', '>=', $hoy)->orderBy('fecha', 'asc')->get();
        $preguntas = Pregunta::where('home', 1)->get();
        $tipos = Tipo::all();
        $mapas = Mapa::all();

        return [$eventos, $eventosDestacados, $preguntas, $tipos, $mapas];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $evento = new Evento;

        if(isset($request->evento)){
            $evento->nombre = $request->evento['nombre'];
            $evento->fecha = $request->evento['fecha'];
            $evento->visibilidad = $request->evento['visibilidad'];
            $evento->destacado = $request->evento['destacado'];
            $evento->mapa_id = $request->evento['mapa_id'];
            $evento->tipo_id = $request->evento['tipo_id'];
            $evento->save();

            
            //obtener categorias asociada al evento

            $categoriasEvento = EventoCategoria::where('evento_id', $request->evento['id'])->get();
            $grupos = [];

            foreach ($categoriasEvento as $key => $categoria) {
                $categoriaEvento = new EventoCategoria;

                $categoriaEvento->categoria_id = $categoria->categoria_id;
                $categoriaEvento->evento_id = $evento->id;

                $categoriaEvento->save();

                //obtener grupos pertenecientes a las categorias obtenidas
                $gruposCategoria = Grupo::where('categoria_evento_id', $categoria->id)->get();

                array_push($grupos, array($categoriaEvento->id, $gruposCategoria));
            }

            $tickets = [];
            
            foreach ($grupos as $key => $grupoCategoria) {

            foreach ($grupoCategoria[1] as $key => $grupo) {

                $grupo_nuevo = new Grupo;

                $grupo_nuevo->numero = $grupo->numero;
                $grupo_nuevo->cantidad_max_disponible = $grupo->cantidad_max_disponible;
                $grupo_nuevo->permitir_ticket_max = $grupo->permitir_ticket_max;
                $grupo_nuevo->categoria_evento_id = $grupoCategoria[0];

                    $grupo_nuevo->save();

                    //obtener tickets  pertenecientes a los grupos obtenidos
                    $ticketsGrupo = Ticket::where('grupo_id', $grupo->id)->get();
                    array_push($tickets, array($grupo_nuevo->id, $ticketsGrupo));

            }
            }

            //volver a crear todos los tickets obtenidos

            foreach ($tickets as $key => $ticketGrupo) {
                foreach ($ticketGrupo[1] as $key => $ticket) {
                    $ticket_nuevo = New Ticket;

                    $ticket_nuevo->sector = $ticket->sector;
                    $ticket_nuevo->juntas = $ticket->juntas;
                    $ticket_nuevo->formato = $ticket->formato;
                    $ticket_nuevo->precioUd = $ticket->precioUd;
                    $ticket_nuevo->precioFinal = $ticket->precioFinal;
                    $ticket_nuevo->grupo_id = $ticketGrupo[0];

                    if($ticket->estado == 3)
                        $ticket_nuevo->estado = 1;
                    else
                        $ticket_nuevo->estado = $ticket->estado;


                    $ticket_nuevo->save();
                }
            }
            
        }else{
            $evento->nombre = $request['nombre'];
            $evento->fecha = $request['fecha'];
            $evento->visibilidad = $request['visibilidad'];
            $evento->destacado = $request['destacado'];
            $evento->mapa_id = $request['mapa_id'];
            $evento->tipo_id = $request['tipo_id'];
            $evento->save();

            
        }

        


        $hoy = date("Y-m-d H:i:s");

        $eventos = Evento::where('destacado', 1)->with('tipo')->with('mapa')->where('fecha', '>=', $hoy)->orderBy('fecha', 'asc')->get();

        return $eventos;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $evento = Evento::where('id', $id)->with('preguntas')->with('mapa')->with('tipo')->first();

        $categorias_evento = DB::table('categoria_evento')
        ->Join('categorias','categorias.id','categoria_evento.categoria_id')
        ->where('categoria_evento.evento_id', $id)
        ->where('categoria_evento.deleted_at', null)
        ->select('categoria_evento.id', 'categorias.nombre', 'categorias.color')
        ->get();
   
        $usuarios = User::where('rol', 'cliente')->get();

        if($evento){
            return [$evento, $usuarios, $categorias_evento];
        }
        else{
            return 'No encontrado';
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // var_dump(isset($request->evento));
  

        $evento = Evento::find($request->evento['id']); 

        $evento->nombre = $request->evento['nombre'];
        $evento->fecha = $request->evento['fecha'];
        $evento->visibilidad = $request->evento['visibilidad'];
        $evento->destacado = $request->evento['destacado'];
        $evento->mapa_id = $request->evento['mapa_id'];
        $evento->tipo_id = $request->evento['tipo_id'];
        $evento->save();
       

        $hoy = date("Y-m-d H:i:s");

        $eventos = Evento::where('destacado', 1)->with('tipo')->with('mapa')->where('fecha', '>=', $hoy)->orderBy('fecha', 'asc')->get();

        return $eventos;
    }

    public function visibilidad(Request $request)
    {
        // dd($request->all());
        $evento = Evento::find($request->evento['id']);
        
        $evento->visibilidad = $request->visibilidad;
        $evento->save();

        $hoy = date("Y-m-d H:i:s");

        $eventos = Evento::where('destacado', 1)->with('tipo')->with('mapa')->where('fecha', '>=', $hoy)->orderBy('fecha', 'asc')->get();
        return $eventos;

    }

    public function destroy($id)
    {
        $evento = Evento::find($id);

        foreach ($evento->eventosCategorias as $key => $value) {
            $grupos = Grupo::where('categoria_evento_id', $value->id)->get();

            foreach ($grupos as $key2 => $grupo) {
                $tickets = Ticket::where('grupo_id', $grupo->id)->get();
                foreach ($tickets as $key3 => $ticket) {
                    Ticket::destroy($ticket->id);
                }
                Grupo::destroy($grupo->id);
            }
            
        }

        $evento->eventosCategorias()->delete();
        Evento::destroy($id);

        $hoy = date("Y-m-d H:i:s");

        $eventosDestacados = Evento::where('destacado', 1)->with('tipo')->with('mapa')->where('fecha', '>=', $hoy)->orderBy('fecha', 'asc')->get();

        return $eventosDestacados;
    }
}
