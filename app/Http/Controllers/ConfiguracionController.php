<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Configuracion;

class ConfiguracionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $configuracion = Configuracion::first();
        
        return $configuracion;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        // $ruta_imagenes = '/home/rrtjzmni4j7c/public_html/imagenes';
        $ruta_imagenes = public_path('imagenes');

        if($request['fondoImg'] != "null"){
            $fondo = $request['fondoImg']->getClientOriginalName();
            $nombre_fondo = 'fondo_'.time() . '.' . $request['fondoImg']->getClientOriginalExtension();
            $request['fondoImg']->move($ruta_imagenes, $nombre_fondo);
        }

        if($request['logoImg'] != "null"){
            $logo = $request['logoImg']->getClientOriginalName();
            $nombre_logo = 'logo_'.time() . '.' . $request['logoImg']->getClientOriginalExtension();
            $request['logoImg']->move($ruta_imagenes, $nombre_logo);
        }


        $configuracion = Configuracion::first();

        if(!$configuracion){
            $configuracion = new Configuracion;

            $configuracion->chat = $request['chat'];
            $configuracion->telefono = $request['telefono'];
            $configuracion->enlace = $request['enlace'];
            $configuracion->email = $request['email'];
            $configuracion->cc = $request['cc'];
            
            if($request['fondoImg'] != "null")
                $configuracion->fondo = $nombre_fondo;

            if($request['logoImg'] != "null")
                $configuracion->logo = $nombre_logo;

            $configuracion->save();
        }else{
            $configuracion->chat = $request['chat'];
            $configuracion->telefono = $request['telefono'];
            $configuracion->enlace = $request['enlace'];
            $configuracion->email = $request['email'];
            $configuracion->cc = $request['cc'];
            
            if($request['fondoImg'] != "null")
                $configuracion->fondo = $nombre_fondo;

            if($request['logoImg'] != "null")
                $configuracion->logo = $nombre_logo;

            $configuracion->save();
        }

        return $configuracion;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
