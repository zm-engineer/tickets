<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::all();

        return $usuarios;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd(intval($request->navegador_unico));

        if(!isset($request['id'])){

            $ruta_imagenes = public_path('imagenes');
            if($request['logo'] != "null"){
                $logo = $request['logo']->getClientOriginalName();
                $nombre_logo = 'logo_'.time() . '.' . $request['logo']->getClientOriginalExtension();
                $request['logo']->move($ruta_imagenes, $nombre_logo);
            }

            $user = new User;

            $user->name = $request->name; 
            $user->email = $request->email;
            if(isset($request['password']))
                $user->password = bcrypt($request->password);
            $user->rol = $request->rol;
            $user->conserje = $request->conserje;
            $user->plus = $request->plus;
            $user->comision = $request->comision;
            $user->reservar = $request->reservar;
            $user->redondeo = $request->redondeo;
            $user->navegador_unico = $request->navegador_unico;
            $user->online = $request->online;

            if(!$request->online){
                $user->ocultar_precios = 0;
            }
           
            if($request['logo'] != "null")
            $user->logo = $nombre_logo;
    
            $user->save();
    
            $users = User::all();
      
            return $users;

        }else{

            $ruta_imagenes = public_path('imagenes');
            if($request->logo != "null"){
                $logo = $request->logo->getClientOriginalName();
                $nombre_logo = 'logo_'.time() . '.' . $request->logo->getClientOriginalExtension();
                $request->logo->move($ruta_imagenes, $nombre_logo);
            }

            $user = User::find($request['id']);

            $user->name = $request->name;
            $user->email = $request->email;
            if(isset($request->password))
                $user->password = bcrypt($request->password);
            $user->rol = $request->rol;
            $user->conserje = $request->conserje;
            $user->plus = $request->plus;
            $user->comision = $request->comision;
            $user->reservar = $request->reservar;
            $user->redondeo = $request->redondeo;

            $user->navegador_unico = $request->navegador_unico;
            $user->online = $request->online;

            if(!$request->online){
                $user->ocultar_precios = 0;
            }
            
            if($request->logo != "null")
                $user->logo = $nombre_logo;
      
            $user->save();

            $users = User::all();
      
            return $users;

        }
    }

    public function update(Request $request, $id)
    {
        // dd($request->all(), $id);
        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        if(isset($request['password']))
            $user->password = bcrypt($request->password);
        $user->rol = $request->rol;
        $user->conserje = $request->conserje;
        $user->plus = $request->plus;
        $user->comision = $request->comision;
        $user->reservar = $request->reservar;
        $user->redondeo = $request->redondeo;
        $user->navegador_unico = intval($request->navegador_unico);
        $user->online = $request->online;
        $user->logo = $request->logo;
  
        $user->save();
  
        return response()->json('Editado correctamente.', 200);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        return response()->json('Usuario eliminado correctamente.', 200);
    }
}
