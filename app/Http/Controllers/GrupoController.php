<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grupo;
use App\Evento;
use App\EventoCategoria;
use App\Categoria;
use App\Ticket;

use Illuminate\Support\Facades\DB;

class GrupoController extends Controller
{
    
    public function index()
    {
        // $grupos = Grupo::all();
        $hoy = date("Y-m-d H:i:s");

        $grupos = DB::table('grupos')
            ->Join('categoria_evento','categoria_evento.id','grupos.categoria_evento_id')
            ->Join('eventos','eventos.id','categoria_evento.evento_id')
            ->where('eventos.fecha', '>=', $hoy)
            ->where('grupos.deleted_at', null)
            ->select('grupos.*')
            ->get();

        $eventosCategorias = EventoCategoria::with('categoria', 'evento')->get();
        // $eventos = Evento::all();
        $categorias = Categoria::all();

        $hoy = date("Y-m-d H:i:s");
        $eventos = Evento::where('fecha', '>=', $hoy)->orderBy('fecha', 'asc')->get();

        return [$grupos, $eventosCategorias, $eventos, $categorias];
    }

    
    public function store(Request $request)
    {
        // dd($request->all(), $request->evento);

        //recorrer eventos y categorias recibidas para crear los grupos
        foreach ($request->evento as $key => $evento) {
            foreach ($request->categoria as $key2 => $categoria) {

                $grupo = new Grupo;

                $grupo->numero = $request->numero;
                $grupo->cantidad_max_disponible = $request->cantidad_max_disponible;
                $grupo->permitir_ticket_max = $request->permitir_ticket_max;
        
                $eventoCategoria = EventoCategoria::where('evento_id', $evento['id'])->where('categoria_id', $categoria['id'])->first();

                if($eventoCategoria){
                    $grupo->categoria_evento_id = $eventoCategoria->id;
                    $grupo->save();
                }else{

                  $creaEventoCat = new EventoCategoria;
                  $creaEventoCat->evento_id = $evento['id'];
                  $creaEventoCat->categoria_id = $categoria['id'];  
                  $creaEventoCat->save();

                  $grupo->categoria_evento_id = $creaEventoCat->id;
                  $grupo->save();

                }
                
            }
        }

        $grupos = Grupo::all();
        return $grupos;
        
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        // dd($request->all(), $id);

        $grupo = Grupo::find($id);
        $grupo->permitir_ticket_max = $request->permitir_ticket_max;
        $grupo->cantidad_max_disponible = $request->cantidad_max_disponible;

        $grupo->save();

        return response()->json('Grupo editado correctamente.', 200);

    }

   
    public function destroy($id)
    {
        //eliminar tickets asociados a ese grupo
        $tickets = Ticket::where('grupo_id', $id)->get();

        foreach ($tickets as $key => $value) {
            Ticket::destroy($value->id);
        }

        Grupo::destroy($id);

        $tickets = Ticket::with('grupo')->get();

        return $tickets;
    }
}
