<?php

namespace App\Helpers\Frontend;
use Mail;
use View;

class EnviosCorreo {

 public static function sendMail($template, $params_template, $to_emails, $subject, $configuracion){

    $cadena = $configuracion->cc;
    $correosConCopia = explode(",", $cadena);

    //desarrollo
    Mail::send($template, ['data' => $params_template],  function($message) use ($to_emails, $subject, $correosConCopia)
    {

          $message->from('info@ticketingemotions.net', 'ticketingemotions');
          $message->to($to_emails)->cc($correosConCopia)->subject($subject);
    });

    //produccion
  /*   try {

       $view = View::make($template, [
             'data' => $params_template
       ]);

       $html = $view->render();
       $to=$to_emails;

       $headers = "From: info@ticketingemotions.net" . "\r\n";
       $headers .= "MIME-Version: 1.0\r\n";
       $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
       $headers .= "Reply-To: $cadena\r\n"

       mail($to, "=?UTF-8?B?".base64_encode($subject)."?=", $html, $headers);

       } catch (Exception $e) {
           dd($e);
     }
     */

 }


}
