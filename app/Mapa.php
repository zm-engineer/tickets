<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;


class Mapa extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'id', 
    ];

    public function evento()
    {
        return $this->hasOne('App\Evento');
    }
}
