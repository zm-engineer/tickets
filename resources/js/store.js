require('es6-promise').polyfill();

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex)

const store = new Vuex.Store({
  //contenedor donde se almacena el estado de la aplicación.
  state: {
    token: localStorage.getItem('access_token') || null,
    usuarioRol: localStorage.getItem('usuarioRol') || null,
    nombreUser: localStorage.getItem('nombreUser') || null,
    configuracion: localStorage.getItem('configuracion') || null,
    mostrarPrecios: localStorage.getItem('usuarioPrecios') || null,
    logoUser: localStorage.getItem('logoUser') || null
  },

  getters: {
    autenticado(state) {
      return state.token !== null
    }
  },

  //funciones que modificarán el estado de la aplicación
  mutations: {
    recuperarToken(state, token) {
      state.token = token
    },
    userRol(state, rol) {
      state.usuarioRol = rol
    },
    nombreUser(state, nombre){
      state.nombreUser = nombre
    },
    logoUser(state, logo){
      state.logoUser = logo
    },
    destruirRol(state, rol) {
      state.usuarioRol = rol
    },
    configuracion(state, configuracion) {
      state.configuracion = configuracion
    }
  },
 
  actions: {
    recuperarToken(context, credentials) {

      return new Promise((resolve, reject) => {
        axios.post('/api/login', {
          username: credentials.username,
          password: credentials.password,
        })
          .then(response => {
            const token = response.data.access_token
            localStorage.setItem('access_token', token)
            context.commit('recuperarToken', token)

            resolve(response)
          })
          .catch(error => {
            //console.log(error)
            reject(error)
          })
      })

    },
    registrarUsuario(context, credentials) {

      return new Promise((resolve, reject) => {
        axios.post('/api/register', {
          nombre: credentials.nombre,
          username: credentials.username,
          password: credentials.password,
          logo: credentials.logo
        })
        .then(response => {
          localStorage.setItem('usuarioCreado', 'Tu cuenta ha sido creada exitosamente.')
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
      })

    },
    destruirToken(context) {

      if (context.getters.autenticado){

        return new Promise((resolve, reject) => {
          axios.post('/api/logout', '', {
              headers: { Authorization: "Bearer " + context.state.token }
            })
            .then(response => {
              localStorage.removeItem('access_token')
              localStorage.removeItem('usuarioRol')
              localStorage.removeItem('logoUser')
              context.commit('destruirRol', null)


              resolve(response)
            })
            .catch(error => {
              //console.log(error)
              localStorage.removeItem('access_token')
              // context.commit('destruirToken')

              reject(error)
            })
        })

      }
    },
    getUsuario(context){

      if(context.getters.autenticado){
        return new Promise((resolve, reject) => {
          axios.get('/api/user', {
            headers: { Authorization: "Bearer " + context.state.token }
          })
          .then(response => {
            let nombreUser = response.data.name 
            let rol = response.data.rol 
            let logo = response.data.logo

            localStorage.setItem('nombreUser', nombreUser)
            localStorage.setItem('usuarioRol', response.data.rol)
            localStorage.setItem('logoUser', response.data.logo)
            localStorage.setItem('usuarioPrecios', response.data.ocultar_precios)
            
            context.commit('userRol', rol)
            context.commit('nombreUser', nombreUser)
            context.commit('logoUser', logo)
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
        })
      }
    },
    getConfiguracion(context){

      if(context.getters.autenticado){
        return new Promise((resolve, reject) => {
          axios.get('/api/configuracion', {
            headers: { Authorization: "Bearer " + context.state.token }
          })
          .then(response => {

            let configuracion = response.data
           
            localStorage.setItem('configuracion', response.data)
            context.commit('configuracion', configuracion)
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
        })
      }
    }
  }
})

export default store
