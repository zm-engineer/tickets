require('./bootstrap');

window.Vue = require('vue');
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.withCredentials = true;
// window.axios.defaults.baseURL = 'https://www.reservadetickets.es';
window.axios.defaults.baseURL = 'http://127.0.0.1:8000';

import BabelPolyfill from "babel-polyfill";  //es6
import Vue from 'vue'
import router from './router'
import store from './store'
import App from './views/App'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css' // Importar los iconos
import 'vue-inner-image-zoom/lib/vue-inner-image-zoom.css';

//Guardian de rutas
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      // esta ruta requiere autenticación, verifique si está conectado
       // si no es así, redirigir a la página de inicio de sesión.
      if (!store.getters.autenticado) {
        next({
          name: 'login',
        })
      } else {
        next()
      }
    }
    else {
      next() //¡asegúrese de llamar siempre a next ()!
    }
  })

import VueFilterDateFormat from 'vue-filter-date-format';
import VueMoment from 'vue-moment'
import moment from 'moment'
import Vuelidate from 'vuelidate'

Vue.use(VueFilterDateFormat);  
Vue.use(Vuetify);
Vue.use(VueMoment, {
  moment,
})
Vue.use(Vuelidate);

import Toastr from 'vue-toastr'
require('vue-toastr/src/vue-toastr.scss');
Vue.use(Toastr, {
    defaultTimeout: 5000,
    defaultProgressBar: true,
    defaultPosition: "toast-top-left",
    defaultCloseOnHover: false
});


const app = new Vue({
    el: '#app',
    vuetify: new Vuetify(),
    components: { App },
    router,
    store,
    BabelPolyfill
});
