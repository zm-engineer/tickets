import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store'; 
Vue.use(VueRouter)

// Pages
import NotFound from './views/NotFound'
import SinPermisos from './views/SinPermisos' 
import Login from './views/Login'
import EventosIndex from './views/eventos/Index'
import EventosDetalle from './views/eventos/Detalle'

import Dashboard from './views/Dashboard'
import PaginaUsuarios from './views/backend/usuarios/PaginaUsuarios'
import NuevoUsuario from './views/backend/usuarios/NuevoUsuario'
import PaginaCategorias from './views/backend/categorias/PaginaCategorias'
import NuevaCategoria from './views/backend/categorias/NuevaCategoria'
import PaginaGrupos from './views/backend/grupos/PaginaGrupos'
import NuevoGrupo from './views/backend/grupos/NuevoGrupo'
import PaginaHistorial from './views/backend/historiales/PaginaHistorial'
import PaginaPreguntas from './views/backend/preguntas/PaginaPreguntas'
import NuevaPregunta from './views/backend/preguntas/NuevaPregunta'
import PaginaConfiguracion from './views/backend/configuracion/PaginaConfiguracion'
import PaginaMapas from './views/backend/mapas/PaginaMapas'
import NuevoMapa from './views/backend/mapas/NuevoMapa'
import NuevoTipo from './views/backend/tipos/NuevoTipo'
import PaginaTipos from './views/backend/tipos/PaginaTipos'
import NuevoEvento from './views/backend/eventos/NuevoEvento'
import PaginaEventos from './views/backend/eventos/PaginaEventos'


import RegisterUser from './views/RegisterUser'


// Routes
const router = new VueRouter({
    mode: 'history',
    linkActiveClass: 'is-active',
    routes: [
        {
           path: '/',
           redirect: { name: 'eventosIndex' }
         },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/admin',
            name: 'dashboard',
            component: Dashboard,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
        },
        {
            path: '/eventos',
            name: 'eventosIndex',
            component: EventosIndex,
            meta: {
               requiresAuth: true,
            }
        },
        {
            path: '/historial',
            name: 'PaginaHistorial',
            component: PaginaHistorial,
            meta: {
               requiresAuth: true,
            }
        },
        {
            path: '/eventos/:id',
            name: 'eventosDetalle',
            component: EventosDetalle,
            meta: {
                requiresAuth: true,
            }
        },
        {
            path: '/sin-permisos',
            name: 'SinPermisos',
            component: SinPermisos,
        },
        {
            path: '/404',
            name: '404',
            component: NotFound,
        },
        {
            path: '*',
            redirect: '/404',
        },
        {
            path: '/register-user',
            name: 'registerUser',
            component: RegisterUser,
            meta: {
                requiresAuth: true,
            }
        },
        {
            path: '/admin/usuarios',
            name: 'PaginaUsuarios',
            component: PaginaUsuarios,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
        },
        {
            path: '/admin/nuevo-usuario',
            name: 'NuevoUsuario',
            component: NuevoUsuario,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
        },
        {
            path: '/admin/categorias',
            name: 'PaginaCategorias',
            component: PaginaCategorias,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
        },
        {
            path: '/admin/nueva-categoria',
            name: 'NuevaCategoria',
            component: NuevaCategoria,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
        },
        {
            path: '/admin/grupos',
            name: 'PaginaGrupos',
            component: PaginaGrupos,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
        },
        {
            path: '/admin/nuevo-grupo',
            name: 'NuevoGrupo',
            component: NuevoGrupo,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
        },
        {
            path: '/admin/preguntas',
            name: 'PaginaPreguntas',
            component: PaginaPreguntas,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
        },
        {
            path: '/admin/nueva-pregunta',
            name: 'NuevaPregunta',
            component: NuevaPregunta,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
        },
        {
            path: '/admin/configuracion',
            name: 'PaginaConfiguracion',
            component: PaginaConfiguracion,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
        },
        {
            path: '/admin/mapas',
            name: 'PaginaMapas',
            component: PaginaMapas,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
        },
        {
            path: '/admin/nuevo-mapa',
            name: 'NuevoMapa',
            component: NuevoMapa,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
           
        },
        {
            path: '/admin/nuevo-tipo-evento',
            name: 'NuevoTipo',
            component: NuevoTipo,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
           
        },
        {
            path: '/admin/tipos-eventos',
            name: 'TipoEvento',
            component: PaginaTipos,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
           
        },
        {
            path: '/admin/nuevo-evento',
            name: 'NuevoEvento',
            component: NuevoEvento,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
           
        },
        {
            path: '/admin/administrar-eventos',
            name: 'PaginaEventos',
            component: PaginaEventos,
            meta: {
                requiresAuth: true,
            },
            beforeEnter: (to, from, next) => {
                if(store.state.usuarioRol == 'admin')
                    next()
                else
                    next({ name: 'SinPermisos' });
            }
           
        },
    ],
});

export default router
