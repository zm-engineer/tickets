<!DOCTYPE html>

<html class="has-aside-left has-aside-expanded">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ticketingemotions</title>
    <link href="{{ url('/css/app.css?v=1.6') }}" rel="stylesheet" />
</head>

<body>
    <div id="app">
        <app></app>
    </div>
    <script src="{{ url('js/app.js?v=1.6') }}"></script>
</body>

</html>
