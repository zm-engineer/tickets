<link rel="stylesheet" href="{{ url('css/app.css') }}">
<div class="header__top" id="barra">
  <div class="container">
    <div class="row">
      <div class="col-md-12 d-flex justify-content-center" data-aos="zoom-in-up">

      </div>
    </div>
  </div>
</div>
<section class="ofrenda__contenido  m-top" style=" padding: 20px 0;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="ofrenda__contenido__intro2" style=" background: #FFFFFF;
                                                        text-align:center;">
          <div class="row">
            <div class="col-lg-6 offset-lg-1">
              <div class="ofrenda__contenido__intro2__p3" style="background-color: #000">
                <p style="  font-size: 30px;
                            line-height: 2.35em;
                            font-family: 'Work+Sans:200', 'work sans', sans-serif;
                            color: #546375;
                            ">
                  <span>
                    <img src="{{ url('imagenes/logo-short-3.png') }}" alt="" 
                    style="
                    text-align:center;
                    width: 50%;
                    border-radius: 100%;
                    margin-top: 20px;">
                  </span>
                </p>
                <h3 style="color:#fff"><span style="text-transform: capitalize; color:#ffff; font-size:15px;">Hola {{ $data->name }}</span></h3>
                <p class="split" style="color: orange;
                                        font-size: 18px;
                                        margin-top: -20px;
                                        font-weight: bold;
                                        padding-bottom:20px;">
              Estás intentando iniciar sesión <strong>a la fecha: </strong>{{ $data->created_at }} 
              </p>
              </div>
              <div class="body">
                <div style="padding-right: 25px;
                                     display: inline-table;">
                    <p>Es necesario que nos confirmes que tu eres quién inicias sesión y no un tercero.</p>
                    <p style="margin-bottom:20px;
                            margin-left:20px; font-size:20px;
                            color: #e73569 !important;">
                        <a href="{{ url('/') }}" target="_blank">
                         Click aquí para continuar con el inicio de sesión.
                        </a>
                    </p>
                </div>
              </div>

              <div class="footer"  style="background-color: #000;
                                          height: 8vh;
                                          align-items: center!important;
                                          align-content: center!important;
                                          display: grid!important;
                                          text-align: center!important;
                                          padding: 10px;" >
                <p style="color: #fff;">© 2021 Ticketing Emotions Todos los derechos reservados.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
