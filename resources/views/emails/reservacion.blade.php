<link rel="stylesheet" href="{{ url('css/app.css') }}">
<div class="header__top" id="barra">
  <div class="container">
    <div class="row">
      <div class="col-md-12 d-flex justify-content-center" data-aos="zoom-in-up">

      </div>
    </div>
  </div>
</div>
<section class="ofrenda__contenido  m-top" style=" padding: 20px 0;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div
          class="ofrenda__contenido__intro2"
          style="background: #FFFFFF;
                border: 1px solid #EBF0F0;
                box-sizing: border-box;
                padding: 30px 30px 30px 30px;
                box-shadow: 0px 4px 27px rgba(1, 15, 38, 0.04);
                text-align:center;">
          <div class="row">
            <div class="col-lg-6 offset-lg-1">
              <div class="ofrenda__contenido__intro2__p3" style="background-color: #000">
                <p style="  font-size: 30px;
                            line-height: 2.35em;
                            font-family: 'Work+Sans:200', 'work sans', sans-serif;
                            color: #546375;
                            ">
                  <span>
                    <img src="{{ url('imagenes/logo-short-3.png') }}" alt="" 
                    style="
                      text-align:center;
                      width: 50%;
                      border-radius: 100%;
                      margin-top: 20px;">
                  </span>
                </p>
                <h3 style="color:#fff"><span style="text-transform: capitalize; color:#ffff"></span></h3>
                <p class="split" style="color: orange;
                                        font-size: 18px;
                                        font-weight: bold;
                                        padding-bottom:20px;">
                Datos de reservación
              </p>
              </div>
              <div class="body">
                <div class="" style="   height: 120vh;
                                        align-items: center;
                                        display: grid;
                                        align-content: center;">
                  <p style="font-size:18px; ">
                    El usuario <strong>{{ $data->usuario->name }} </strong> ha hecho la siguiente reservación:
                  </p>
                  <span style="font-size:16px;">
                   <strong style="color: orange;">
                   Evento: </strong> <span>{{ $data->evento->nombre }}
                   <br>
                   con fecha {{ $data->evento->fecha }}</span>
                  </span>
                  <span style="font-size:16px;">
                   <strong style="color: orange;">
                   Ticket: </strong> 
                   <span>
                     {{ $data->ticket->grupo->numero }}
                    <br>
                    Sector: {{ $data->ticket->sector }}
                    <br>
                    Juntas: {{ $data->ticket->juntas }}
                    <br>
                    Formato: {{ $data->ticket->formato }}
                    <br>
                    <strong>Precio neto:</strong> {{ $data->ticket->precioUd }} €

                    @php
                      $descuentoPlus = $data->ticket->precioUd * $data->usuario->plus;
                      $precioNetoPlus = $data->ticket->precioUd + $descuentoPlus;
                      $descuentoComision = $precioNetoPlus * $data->usuario->comision;
                      $precioTotalUnitario = $precioNetoPlus + $descuentoComision;
                    @endphp

                    <br>
                   <strong>+{{ $data->usuario->plus*100 }}% plus:</strong> {{ $data->ticket->precioUd .' + '. $descuentoPlus }} = {{ $precioNetoPlus }} €
                    <br> 
                   <strong>+{{  $data->usuario->comision*100 }}% comisión:</strong> {{ $precioNetoPlus .' + '. $descuentoComision }} = {{ $precioTotalUnitario }} €
                    <br>
                  <strong>Precio Total:</strong> {{ $data->ticket->grupo->numero * $precioTotalUnitario }} €
                   </span>
                  </span>
                  <span style="font-size:16px;">
                   <strong style="color: orange;">
                   Conserje: </strong> <span>{{ $data->conserje }}</span>
                  </span>
                  @php
                    $fecha_mod = strtotime ( '+1 hour' , strtotime ($data->created_at) );
                    $fecha = date("Y-m-d H:i:s", $fecha_mod);
                  @endphp
                  <span style="font-size:16px;">
                   <strong style="color: orange;">
                   Fecha de reserva: </strong> <span>{{ $fecha }}</span>
                  </span>
                </div>
              </div>

              <div class="footer"  style="background-color: #000;
                                          height: 10vh!important;
                                          align-items: center!important;
                                          align-content: center!important;
                                          display: grid!important;
                                          text-align: center!important;
                                          padding: 10px;!important" >
                <p style="color: #fff;">© 2021 Ticketing Emotions Todos los derechos reservados.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
