<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class eventosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('eventos')->insert([
            'nombre' => 'Real madrid vs Eibar',
            'fecha' => date('Y-m-d H:i:s'),
            'visibilidad' => 1,
            'destacado' => 0,
            'estado' => 1
        ]);
    }
}
