<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class gruposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grupos')->insert([
            'numero' => 5,
            'cantidad_max_disponible' => 10,
            'permitir_ticket_max' => 0,
            'categoria_evento_id' => 1,
            'estado' => 1,
        ]);

        DB::table('grupos')->insert([
            'numero' => 4,
            'cantidad_max_disponible' => 12,
            'permitir_ticket_max' => 1,
            'categoria_evento_id' => 1,
            'estado' => 1,
        ]);

        DB::table('grupos')->insert([
            'numero' => 3,
            'cantidad_max_disponible' => 3,
            'permitir_ticket_max' => 1,
            'categoria_evento_id' => 1,
            'estado' => 1,
        ]);

        DB::table('grupos')->insert([
            'numero' => 5,
            'cantidad_max_disponible' => 10,
            'permitir_ticket_max' => 1,
            'categoria_evento_id' => 2,
            'estado' => 1,
        ]);

        DB::table('grupos')->insert([
            'numero' => 6,
            'cantidad_max_disponible' => 12,
            'permitir_ticket_max' => 0,
            'categoria_evento_id' => 2,
            'estado' => 1,
        ]);

        DB::table('grupos')->insert([
            'numero' => 3,
            'cantidad_max_disponible' => 3,
            'permitir_ticket_max' => 0,
            'categoria_evento_id' => 2,
            'estado' => 1,
        ]);

        DB::table('grupos')->insert([
            'numero' => 6,
            'cantidad_max_disponible' => 12,
            'permitir_ticket_max' => 0,
            'categoria_evento_id' => 3,
            'estado' => 1,
        ]);

        DB::table('grupos')->insert([
            'numero' => 4,
            'cantidad_max_disponible' => 12,
            'permitir_ticket_max' => 0,
            'categoria_evento_id' => 4,
            'estado' => 1,
        ]);

    }
}
