<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ticketsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tickets')->insert([
            'sector' => 159,
            'juntas' => 'Si',
            'formato' => 'Papel',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 1,
            'estado' => 1
        ]);

        DB::table('tickets')->insert([
            'sector' => 237,
            'juntas' => 'Diagonal',
            'formato' => 'Móvil',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 1,
            'estado' => 2
        ]);

        DB::table('tickets')->insert([
            'sector' => 237,
            'juntas' => 'Diagonal',
            'formato' => 'Tarjeta',
            'precioUd' => 150,
            'precioFinal' => 0,
            'grupo_id' => 1,
            'estado' => 2
        ]);

        // grupo 2

        DB::table('tickets')->insert([
            'sector' => 159,
            'juntas' => 'Si',
            'formato' => 'Papel',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 2,
            'estado' => 1
        ]);

        DB::table('tickets')->insert([
            'sector' => 237,
            'juntas' => 'Diagonal',
            'formato' => 'Móvil',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 2,
            'estado' => 2
        ]);

        DB::table('tickets')->insert([
            'sector' => 237,
            'juntas' => 'Diagonal',
            'formato' => 'Tarjeta',
            'precioUd' => 150,
            'precioFinal' => 0,
            'grupo_id' => 2,
            'estado' => 2
        ]);

        // grupo 3

        DB::table('tickets')->insert([
            'sector' => 159,
            'juntas' => 'Si',
            'formato' => 'Papel',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 3,
            'estado' => 1
        ]);

        DB::table('tickets')->insert([
            'sector' => 237,
            'juntas' => 'Diagonal',
            'formato' => 'Móvil',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 3,
            'estado' => 2
        ]);

        
        // grupo 4

        DB::table('tickets')->insert([
            'sector' => 159,
            'juntas' => 'Si',
            'formato' => 'Papel',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 4,
            'estado' => 1
        ]);

        DB::table('tickets')->insert([
            'sector' => 237,
            'juntas' => 'Diagonal',
            'formato' => 'Móvil',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 4,
            'estado' => 2
        ]);

        
        // grupo 5

        DB::table('tickets')->insert([
            'sector' => 159,
            'juntas' => 'Si',
            'formato' => 'Papel',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 5,
            'estado' => 1
        ]);

        DB::table('tickets')->insert([
            'sector' => 237,
            'juntas' => 'Diagonal',
            'formato' => 'Móvil',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 5,
            'estado' => 2
        ]);

        
        // grupo 6

        DB::table('tickets')->insert([
            'sector' => 159,
            'juntas' => 'Si',
            'formato' => 'Papel',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 6,
            'estado' => 1
        ]);

        DB::table('tickets')->insert([
            'sector' => 237,
            'juntas' => 'Diagonal',
            'formato' => 'Móvil',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 6,
            'estado' => 2
        ]);

        
        // grupo 7

        DB::table('tickets')->insert([
            'sector' => 159,
            'juntas' => 'Si',
            'formato' => 'Papel',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 7,
            'estado' => 1
        ]);

        DB::table('tickets')->insert([
            'sector' => 237,
            'juntas' => 'Diagonal',
            'formato' => 'Móvil',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 7,
            'estado' => 2
        ]);

        
        // grupo 8

        DB::table('tickets')->insert([
            'sector' => 159,
            'juntas' => 'Si',
            'formato' => 'Papel',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 8,
            'estado' => 1
        ]);

        DB::table('tickets')->insert([
            'sector' => 237,
            'juntas' => 'Diagonal',
            'formato' => 'Móvil',
            'precioUd' => 120,
            'precioFinal' => 0,
            'grupo_id' => 8,
            'estado' => 2
        ]);

     
    }
}
