<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class eventosCategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categoria_evento')->insert([
            'evento_id' => 1,
            'categoria_id' => 1
        ]);
        DB::table('categoria_evento')->insert([
            'evento_id' => 1,
            'categoria_id' => 2
        ]);
        DB::table('categoria_evento')->insert([
            'evento_id' => 1,
            'categoria_id' => 3
        ]);
        DB::table('categoria_evento')->insert([
            'evento_id' => 1,
            'categoria_id' => 4
        ]);
    }
}
