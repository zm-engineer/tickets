<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class categoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('categorias')->insert([
            'nombre' => 'Premium',
            'estado' => 1
        ]);

        DB::table('categorias')->insert([
            'nombre' => 'Cat 1',
            'estado' => 1
        ]);

        DB::table('categorias')->insert([
            'nombre' => 'Cat 2',
            'estado' => 1
        ]);

        DB::table('categorias')->insert([
            'nombre' => 'Cat 3',
            'estado' => 1
        ]);
       
    }
}
