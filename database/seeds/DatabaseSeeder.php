<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            eventosSeeder::class,
            categoriasSeeder::class,
            eventosCategoriasSeeder::class,
            gruposSeeder::class,
            ticketsSeeder::class,
        ]);
    }
}
