<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrupoTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('numero');
            $table->integer('cantidad_max_disponible');
            $table->boolean('permitir_ticket_max')->default(1);
            $table->unsignedInteger('categoria_evento_id');
            $table->boolean('estado')->default(1);

            $table->foreign('categoria_evento_id')->references('id')->on('evento_categorias')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupo_tickets');
    }
}
