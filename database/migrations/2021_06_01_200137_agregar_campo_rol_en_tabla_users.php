<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregarCampoRolEnTablaUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('rol', 50)->default('cliente')->after('email');
            $table->string('conserje', 255)->nullable()->after('rol');
            $table->float('plus')->nullable()->after('conserje');
            $table->float('comision')->nullable()->after('plus');
            $table->boolean('ocultar_precios')->default(0)->after('comision');
            $table->boolean('reservar')->default(1)->after('ocultar_precios');
            $table->string('tiempo_expiracion', 255)->nullable()->after('remember_token');
            $table->string('navegador_unico', 255)->nullable()->after('tiempo_expiracion');
            $table->boolean('online')->default(0)->after('navegador_unico');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('rol');
            $table->dropColumn('conserje');
            $table->dropColumn('plus');
            $table->dropColumn('comision');
            $table->dropColumn('ocultar_precios');
            $table->dropColumn('reservar');
            $table->dropColumn('tiempo_expiracion');
            $table->dropColumn('navegador_unico');
            $table->dropColumn('online');
        });
    }
}
