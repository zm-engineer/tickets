<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenombrarTablaDeEventoCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('evento_categorias', 'categoria_evento');
        Schema::rename('grupo_tickets', 'grupos');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('categoria_evento', 'evento_categorias');
        Schema::rename('grupos', 'grupo_tickets');
        
    }
}
