<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AgregandoCamposPrecioUdFinalAReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->integer('numero')->nullable()->after('conserje');
            $table->float('precioUd')->nullable()->after('numero');
            $table->float('plus')->nullable()->after('precioUd');
            $table->float('comision')->nullable()->after('plus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropColumn('numero');
            $table->dropColumn('precioUd');
            $table->dropColumn('plus');
            $table->dropColumn('comision');
        });
    }
}
