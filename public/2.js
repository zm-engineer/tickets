(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      ticketAreservar: null,
      notaFormato: null,
      conserjeReserva: null,
      enlace: null,
      reservando: false
    };
  },
  props: {
    categorias: {
      type: Array,
      "default": []
    },
    tickets: {
      type: Array,
      "default": []
    }
  },
  beforeCreate: function beforeCreate() {
    var _this = this;

    axios.get('/api/configuracion').then(function (response) {
      _this.enlace = response.data.enlace;
    });
  },
  computed: {},
  methods: {
    arrTickets: function arrTickets(id) {
      var _this2 = this;

      this.$parent.catId = id;
      this.$parent.gruposFiltrados = this.tickets.filter(function (ticket) {
        return ticket.grupo.categoria_evento_id == _this2.$parent.catId && ticket.estado == 1;
      });
      this.$parent.ticketsStock = this.tickets.filter(function (ticket) {
        return ticket.grupo.categoria_evento_id == _this2.$parent.catId && ticket.estado == 2;
      });
      this.$parent.gruposFiltrados.sort(function (a, b) {
        return b.grupo.numero - a.grupo.numero;
      });
      var obtenerTicketReservado;
      this.$parent.gruposReservados = [];
      this.$parent.reservasDeUsuarioAutenticado.forEach(function (elemento) {
        obtenerTicketReservado = _this2.tickets.find(function (ticket) {
          return ticket.grupo.categoria_evento_id == _this2.$parent.catId && ticket.id == elemento.ticket_id;
        });
        if (obtenerTicketReservado) _this2.$parent.gruposReservados.push(obtenerTicketReservado);
      });
      this.$parent.sinDatos = null;
      if (this.$parent.gruposFiltrados.length == 0) this.$parent.sinDatos = 'Sin tickets disponibles';
    },
    mostrarTickets: function mostrarTickets(id) {
      if (id == this.$parent.catId) {
        return true;
      }
    },
    reservar: function reservar(ticket) {
      this.ticketAreservar = ticket.id;

      if (ticket.formato == 'Papel') {
        this.notaFormato = 'El cliente podrá quedarse con el ticket original de recuerdo';
      } else if (ticket.formato == 'Móvil') {
        this.notaFormato = 'El cliente necesitará un móvil smartPhone desde donde se descargará los tickets electrónicos. Tutorial para la descarga ver video en este ';
      } else {
        this.notaFormato = 'Las tarjetas deben ser devueltas la misma noche del evento. Solo serán válidas para clientes Europeos y Latinoamericanos. Otras nacionalidades consultar';
      }
    },
    finalizarReserva: function finalizarReserva(ticket) {
      var _this3 = this;

      if (this.conserjeReserva) {
        this.reservando = true;
        axios.post('/api/reservacion', {
          usuario: this.$parent.usuarioAutenticado.id,
          ticket: ticket.id,
          evento: this.$parent.evento.id,
          conserje: this.conserjeReserva
        }, {
          headers: {
            Authorization: "Bearer " + this.$store.state.token
          }
        }).then(function (response) {
          if (response.status == 200) {
            _this3.ticketAreservar = null;
            _this3.conserjeReserva = null;
            _this3.reservando = false;

            if (response.data == 'Éste ticket ya ha sido reservado o no está disponible.') {
              alert(response.data);
            } else {
              //se vuelve a filtrar el ticket ya que no estara disponible
              _this3.$parent.obtenerTicketsFiltrados(_this3.$parent.catId); //pasar el ticket a grupos reservados
              //debo actualizar las reservas y luego filtrar para
              //obtener el grupoReservados por categoria


              _this3.$parent.reservasDeUsuarioAutenticado = response.data;
              var obtenerTicketReservado;
              _this3.$parent.gruposReservados = [];

              _this3.$parent.reservasDeUsuarioAutenticado.forEach(function (elemento) {
                obtenerTicketReservado = _this3.tickets.find(function (ticket) {
                  return ticket.grupo.categoria_evento_id == _this3.$parent.catId && ticket.id == elemento.ticket_id;
                });
                if (obtenerTicketReservado) _this3.$parent.gruposReservados.push(obtenerTicketReservado);
              }); //pasar de stock a disponible


              setTimeout(function () {
                _this3.$emit('deStockAdisponible', ticket.grupo);
              }, 1000);
            }
          }
        })["catch"](function (error) {
          console.log(error);
        });
      }
    },
    obtenerCategoriaInicial: function obtenerCategoriaInicial() {
      this.catId = this.categorias ? this.categorias[0].id : null;
      this.$parent.obtenerTicketsFiltrados(this.catId);
    },
    precioUnitario: function precioUnitario(ticket) {
      var resultado = this.obtenerPrecioFinal(ticket) / ticket.grupo.numero;
      return parseInt(resultado);
    },
    obtenerPrecioFinal: function obtenerPrecioFinal(ticket) {
      var numero = Number(ticket.grupo.numero);
      var precioUd = Number(ticket.precioUd); //sumar al multiplicado los porcentajes de comision y plus

      var usuario = this.$parent.usuarioAutenticado;
      var neto, plus, precioPlus, comision, precioComision;

      if (precioUd % 1 != 0) {
        neto = parseFloat(precioUd);
      } else {
        neto = parseInt(precioUd);
      }

      plus = neto * usuario.plus;

      if (plus % 1 != 0) {
        plus = parseFloat(plus).toFixed(1);
        precioPlus = neto + parseFloat(plus);
      } else {
        plus = parseInt(plus);
        precioPlus = neto + parseInt(plus);
      }

      comision = precioPlus * usuario.comision;

      if (comision % 1 != 0) {
        comision = parseFloat(comision).toFixed(2);
        precioComision = precioPlus + parseFloat(comision);
      } else {
        comision = parseInt(comision);
        precioComision = precioPlus + parseInt(comision);
      }

      var resultadoConComisiones = precioComision; //CALCULAR REDONDEO

      var ultimaCifra, redondeoRealizado, resta;

      if (usuario.redondeo == 5) {
        // if(resultadoConComisiones % 1 == 0)
        //   ultimaCifra = resultadoConComisiones.toFixed(2).toString().slice(-4)
        // else
        //   ultimaCifra = resultadoConComisiones.toFixed(2).toString().slice(-4)
        ultimaCifra = resultadoConComisiones.toFixed(2).toString().slice(-4);

        if (Number(ultimaCifra) > 5) {
          resta = 10 - Number(ultimaCifra);
        } else {
          resta = 5 - Number(ultimaCifra);
        }

        redondeoRealizado = resultadoConComisiones + resta;
      } else if (usuario.redondeo == 1) {
        ultimaCifra = 0;
        if (resultadoConComisiones % 1 != 0) ultimaCifra = resultadoConComisiones.toFixed(2).toString().slice(-3);
        resta = resultadoConComisiones - Number(ultimaCifra);
        redondeoRealizado = resta + 1;
      } else {
        ultimaCifra = 0;
        if (resultadoConComisiones % 1 != 0) ultimaCifra = resultadoConComisiones.toFixed(2).toString().slice(-3);
        resta = resultadoConComisiones - Number(ultimaCifra);
        redondeoRealizado = resta;
      }

      var multiplicado = numero * redondeoRealizado.toFixed(2); // console.log(precioUd, resultadoConComisiones.toFixed(2), redondeoRealizado.toFixed(2), multiplicado)

      return parseInt(multiplicado);
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=style&index=0&id=6bac26b8&scoped=true&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=style&index=0&id=6bac26b8&scoped=true&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.bordes-left[data-v-6bac26b8]{\n   border-left: 2px solid;\n}\n.bordes-right[data-v-6bac26b8]{\n   border-right: 2px solid;\n}\n.bordes-bottom[data-v-6bac26b8]{\n   border-bottom: 2px solid;\n}\n.bordes-header[data-v-6bac26b8]{\n   border: 2px solid;\n   border-bottom: 2px solid !important;\n   background-color:khaki;\n}\n.bordes-bottom-clarito[data-v-6bac26b8]{\n   border-bottom:1px solid #e0e0e0;\n}\ntable>tbody>tr>td[data-v-6bac26b8]{\n   padding: 0px 0px!important;\n}\n.nombre-categoria[data-v-6bac26b8]{\n   \n   color: black !important;\n   font-size: 14px !important;\n}\ntd[data-v-6bac26b8]{\n   border: 1px solid #b5b3aa;\n}\n.colorReservar[data-v-6bac26b8]{\n   background-color: gold;\n}\n.colorReservar[data-v-6bac26b8]:hover{\n   background-color: #0eab29b8;\n}\n.colorReservarActivado[data-v-6bac26b8]{\n   background-color: #0eab29b8;\n}\n.fuente-elDoble[data-v-6bac26b8]{\n   font-size: 1.2em;\n}\n\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=style&index=0&id=6bac26b8&scoped=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=style&index=0&id=6bac26b8&scoped=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./TicketsUsuarioComponent.vue?vue&type=style&index=0&id=6bac26b8&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=style&index=0&id=6bac26b8&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=template&id=6bac26b8&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=template&id=6bac26b8&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("v-simple-table", {
    attrs: { dense: "" },
    scopedSlots: _vm._u([
      {
        key: "default",
        fn: function() {
          return [
            _vm._l(_vm.categorias, function(item, i) {
              return _c(
                "thead",
                { key: i },
                [
                  _c(
                    "tr",
                    {
                      on: {
                        click: function($event) {
                          return _vm.arrTickets(item.id)
                        }
                      }
                    },
                    [
                      _c(
                        "th",
                        {
                          staticClass:
                            "text-center nombre-categoria text-uppercase",
                          style:
                            "background-color: " +
                            item.color +
                            "; cursor: pointer; border: 2px solid;",
                          attrs: { colspan: "6" }
                        },
                        [
                          _vm._v(
                            "\n           " +
                              _vm._s(item.nombre) +
                              "\n         "
                          )
                        ]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "tr",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.mostrarTickets(item.id),
                          expression: "mostrarTickets(item.id)"
                        }
                      ]
                    },
                    [
                      _c("th", { staticClass: "text-center bordes-header" }, [
                        _c("div", { staticClass: "mt-1" }, [
                          _vm._v("\n             N° Tickets\n           ")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("th", { staticClass: "text-center bordes-header" }, [
                        _c("div", { staticClass: "mt-1" }, [_vm._v("Sector")])
                      ]),
                      _vm._v(" "),
                      _c("th", { staticClass: "text-center bordes-header" }, [
                        _c("div", { staticClass: "mt-1" }, [_vm._v("Juntas")])
                      ]),
                      _vm._v(" "),
                      _c("th", { staticClass: "text-center bordes-header" }, [
                        _c("div", { staticClass: "mt-1" }, [_vm._v("Formato")])
                      ]),
                      _vm._v(" "),
                      _c("th", { staticClass: "text-center bordes-header" }, [
                        _c("div", { staticClass: "mt-1" }, [
                          _vm._v("Precio / ud")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("th", { staticClass: "text-center bordes-header" }, [
                        _c("div", { staticClass: "mt-1" }, [
                          _vm._v("Precio Total")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("th", { attrs: { colspan: "3" } })
                    ]
                  ),
                  _vm._v(" "),
                  _vm.$parent.catId == item.id && _vm.$parent.sinDatos
                    ? _c("tr", [
                        _c(
                          "th",
                          {
                            staticClass: "text-center",
                            staticStyle: { padding: "0", border: "2px solid" },
                            attrs: { colspan: "6" }
                          },
                          [
                            _vm._v(
                              "\n           " +
                                _vm._s(_vm.$parent.sinDatos) +
                                "\n         "
                            )
                          ]
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.$parent.cargandoTickets
                    ? _c("v-progress-linear", {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.$parent.catId == item.id,
                            expression: "$parent.catId == item.id"
                          }
                        ],
                        attrs: { indeterminate: "", color: "teal" }
                      })
                    : _vm._e(),
                  _vm._v(" "),
                  _vm._l(_vm.$parent.gruposFiltrados, function(item2, idx) {
                    return [
                      _c(
                        "tr",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: _vm.$parent.catId == item.id,
                              expression: "$parent.catId == item.id"
                            }
                          ],
                          key: idx,
                          style:
                            idx % 2 != 0
                              ? "background-color: rgb(240 230 140);"
                              : ""
                        },
                        [
                          _c(
                            "td",
                            {
                              class:
                                idx == _vm.$parent.gruposFiltrados.length - 1
                                  ? "text-center bordes-left"
                                  : "text-center bordes-left"
                            },
                            [
                              _vm._v(
                                "\n               " +
                                  _vm._s(item2.grupo.numero) +
                                  "\n             "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              class:
                                idx == _vm.$parent.gruposFiltrados.length - 1
                                  ? "text-center"
                                  : "text-center"
                            },
                            [
                              _vm._v(
                                "\n               " +
                                  _vm._s(item2.sector) +
                                  "\n             "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              class:
                                idx == _vm.$parent.gruposFiltrados.length - 1
                                  ? "text-center"
                                  : "text-center"
                            },
                            [
                              _vm._v(
                                "\n               " +
                                  _vm._s(item2.juntas) +
                                  "\n             "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              class:
                                idx == _vm.$parent.gruposFiltrados.length - 1
                                  ? "text-center"
                                  : "text-center"
                            },
                            [
                              _vm._v(
                                "\n               " +
                                  _vm._s(item2.formato) +
                                  "\n             "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              class:
                                idx == _vm.$parent.gruposFiltrados.length - 1
                                  ? "text-center"
                                  : "text-center"
                            },
                            [
                              item2.precioUd != 0
                                ? _c("span", [
                                    _vm._v(
                                      "\n               " +
                                        _vm._s(
                                          _vm.precioUnitario(item2) + " €"
                                        ) +
                                        " \n             "
                                    )
                                  ])
                                : _c("span", [
                                    _vm._v("\n               - \n             ")
                                  ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              class:
                                idx == _vm.$parent.gruposFiltrados.length - 1
                                  ? "text-center bordes-right"
                                  : "text-center bordes-right"
                            },
                            [
                              item2.precioUd != 0
                                ? _c("span", [
                                    _vm._v(
                                      "\n                 " +
                                        _vm._s(
                                          _vm.obtenerPrecioFinal(item2) + " €"
                                        ) +
                                        "\n               "
                                    )
                                  ])
                                : _c("span", [
                                    _vm._v(
                                      "\n                 -\n               "
                                    )
                                  ])
                            ]
                          ),
                          _vm._v(" "),
                          item2.precioUd != 0
                            ? _c(
                                "td",
                                {
                                  class:
                                    item2.id == _vm.ticketAreservar
                                      ? "text-center colorReservarActivado"
                                      : "text-center colorReservar",
                                  staticStyle: { cursor: "pointer" },
                                  on: {
                                    click: function($event) {
                                      return _vm.reservar(item2)
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n               Reservar\n             "
                                  )
                                ]
                              )
                            : item2.precioUd == 0
                            ? _c("td", { staticClass: "text-center" }, [
                                _vm._v(" Consultar por tlf")
                              ])
                            : _vm._e()
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "tr",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value:
                                item2.id == _vm.ticketAreservar &&
                                _vm.$parent.catId == item.id,
                              expression:
                                "item2.id == ticketAreservar && $parent.catId == item.id"
                            }
                          ]
                        },
                        [
                          _c(
                            "td",
                            {
                              staticStyle: { "border-left": "2px solid" },
                              attrs: { colspan: "3" }
                            },
                            [
                              _c("v-text-field", {
                                attrs: { placeholder: "Nombre del conserje" },
                                model: {
                                  value: _vm.conserjeReserva,
                                  callback: function($$v) {
                                    _vm.conserjeReserva = $$v
                                  },
                                  expression: "conserjeReserva"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          !_vm.reservando
                            ? _c(
                                "td",
                                {
                                  style: _vm.conserjeReserva
                                    ? "background-color: #0eab29b8; cursor:pointer;"
                                    : "",
                                  attrs: { colspan: "2" },
                                  on: {
                                    click: function($event) {
                                      return _vm.finalizarReserva(item2)
                                    }
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticStyle: {
                                        display: "grid",
                                        height: "100%",
                                        "align-items": "center",
                                        "text-align": "center"
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n               Click aquí para finalizar la reserva\n             "
                                      )
                                    ]
                                  )
                                ]
                              )
                            : _c(
                                "td",
                                {
                                  style: _vm.conserjeReserva
                                    ? "background-color: #0eab29b8; cursor:pointer;"
                                    : "",
                                  attrs: { colspan: "2" }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticStyle: {
                                        display: "grid",
                                        height: "100%",
                                        "align-items": "center",
                                        "text-align": "center"
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n               Reservando...\n             "
                                      )
                                    ]
                                  )
                                ]
                              ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              staticStyle: {
                                "background-color": "darkgray",
                                cursor: "pointer",
                                "border-right": "2px solid"
                              },
                              on: {
                                click: function($event) {
                                  _vm.ticketAreservar = null
                                }
                              }
                            },
                            [
                              _c(
                                "div",
                                {
                                  staticStyle: {
                                    display: "grid",
                                    height: "100%",
                                    "align-items": "center",
                                    "text-align": "center"
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n               Atrás\n             "
                                  )
                                ]
                              )
                            ]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "tr",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value:
                                item2.id == _vm.ticketAreservar &&
                                _vm.$parent.catId == item.id,
                              expression:
                                "item2.id == ticketAreservar && $parent.catId == item.id"
                            }
                          ]
                        },
                        [
                          _c(
                            "td",
                            {
                              staticStyle: {
                                "border-left": "2px solid",
                                "border-right": "2px solid"
                              },
                              attrs: { colspan: "6" }
                            },
                            [
                              _c("span", [
                                _c(
                                  "strong",
                                  { staticStyle: { color: "red" } },
                                  [
                                    _vm._v(
                                      "\n                 Importante:\n               "
                                    )
                                  ]
                                ),
                                _vm._v(
                                  "\n               " +
                                    _vm._s(_vm.notaFormato) +
                                    "\n\n               "
                                ),
                                item2.formato == "Móvil"
                                  ? _c(
                                      "a",
                                      {
                                        staticClass:
                                          "text-decoration-underline",
                                        attrs: {
                                          href: _vm.enlace,
                                          target: "_blank"
                                        }
                                      },
                                      [_vm._v("enlace")]
                                    )
                                  : _vm._e()
                              ])
                            ]
                          )
                        ]
                      )
                    ]
                  }),
                  _vm._v(" "),
                  _vm._l(_vm.$parent.gruposReservados, function(item3) {
                    return [
                      _c(
                        "tr",
                        {
                          directives: [
                            {
                              name: "show",
                              rawName: "v-show",
                              value: _vm.$parent.catId == item.id,
                              expression: "$parent.catId == item.id"
                            }
                          ],
                          style: "background-color: rgb(61 185 83);"
                        },
                        [
                          _c("td", { class: "text-center bordes-left" }, [
                            _vm._v(
                              "\n             " +
                                _vm._s(item3.grupo.numero) +
                                "\n           "
                            )
                          ]),
                          _vm._v(" "),
                          _c("td", { class: "text-center" }, [
                            _vm._v(
                              "\n             " +
                                _vm._s(item3.sector) +
                                "\n           "
                            )
                          ]),
                          _vm._v(" "),
                          _c("td", { class: "text-center" }, [
                            _vm._v(
                              "\n             " +
                                _vm._s(item3.juntas) +
                                "\n           "
                            )
                          ]),
                          _vm._v(" "),
                          _c("td", { class: "text-center" }, [
                            _vm._v(
                              "\n             " +
                                _vm._s(item3.formato) +
                                "\n           "
                            )
                          ]),
                          _vm._v(" "),
                          _c("td", { class: "text-center" }, [
                            item3.precioUd != 0
                              ? _c("span", [
                                  _vm._v(
                                    "\n               " +
                                      _vm._s(_vm.precioUnitario(item3) + " €") +
                                      "\n             "
                                  )
                                ])
                              : _c("span", [
                                  _vm._v("\n               -\n             ")
                                ])
                          ]),
                          _vm._v(" "),
                          _c("td", { class: "text-center bordes-right" }, [
                            item3.precioUd != 0
                              ? _c("span", [
                                  _vm._v(
                                    "\n               " +
                                      _vm._s(
                                        _vm.obtenerPrecioFinal(item3) + " €"
                                      ) +
                                      "\n             "
                                  )
                                ])
                              : _c("span", [
                                  _vm._v("\n               -\n             ")
                                ])
                          ])
                        ]
                      )
                    ]
                  })
                ],
                2
              )
            }),
            _vm._v(" "),
            _c("thead", [
              _c("tr", [
                _c("th", {
                  staticStyle: {
                    "border-top": "2px solid",
                    "border-bottom": "0px"
                  },
                  attrs: { colspan: "6" }
                })
              ])
            ])
          ]
        },
        proxy: true
      }
    ])
  })
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/componentes/TicketsUsuarioComponent.vue":
/*!**************************************************************!*\
  !*** ./resources/js/componentes/TicketsUsuarioComponent.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TicketsUsuarioComponent_vue_vue_type_template_id_6bac26b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TicketsUsuarioComponent.vue?vue&type=template&id=6bac26b8&scoped=true& */ "./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=template&id=6bac26b8&scoped=true&");
/* harmony import */ var _TicketsUsuarioComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TicketsUsuarioComponent.vue?vue&type=script&lang=js& */ "./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _TicketsUsuarioComponent_vue_vue_type_style_index_0_id_6bac26b8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./TicketsUsuarioComponent.vue?vue&type=style&index=0&id=6bac26b8&scoped=true&lang=css& */ "./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=style&index=0&id=6bac26b8&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _TicketsUsuarioComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TicketsUsuarioComponent_vue_vue_type_template_id_6bac26b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TicketsUsuarioComponent_vue_vue_type_template_id_6bac26b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "6bac26b8",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/componentes/TicketsUsuarioComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsUsuarioComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./TicketsUsuarioComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsUsuarioComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=style&index=0&id=6bac26b8&scoped=true&lang=css&":
/*!***********************************************************************************************************************!*\
  !*** ./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=style&index=0&id=6bac26b8&scoped=true&lang=css& ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsUsuarioComponent_vue_vue_type_style_index_0_id_6bac26b8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./TicketsUsuarioComponent.vue?vue&type=style&index=0&id=6bac26b8&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=style&index=0&id=6bac26b8&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsUsuarioComponent_vue_vue_type_style_index_0_id_6bac26b8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsUsuarioComponent_vue_vue_type_style_index_0_id_6bac26b8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsUsuarioComponent_vue_vue_type_style_index_0_id_6bac26b8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsUsuarioComponent_vue_vue_type_style_index_0_id_6bac26b8_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=template&id=6bac26b8&scoped=true&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=template&id=6bac26b8&scoped=true& ***!
  \*********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsUsuarioComponent_vue_vue_type_template_id_6bac26b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./TicketsUsuarioComponent.vue?vue&type=template&id=6bac26b8&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsUsuarioComponent.vue?vue&type=template&id=6bac26b8&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsUsuarioComponent_vue_vue_type_template_id_6bac26b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsUsuarioComponent_vue_vue_type_template_id_6bac26b8_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);