(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsAdminComponent.vue?vue&type=style&index=0&id=746f853a&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/componentes/TicketsAdminComponent.vue?vue&type=style&index=0&id=746f853a&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.colorSegunEstado1[data-v-746f853a]{\n   background: #ffe47f;\n}\n.colorSegunEstado2[data-v-746f853a]{\n   background: #d4d0d0fc;\n}\n.colorSegunEstado3[data-v-746f853a]{\n   background: #8bc34a;\n}\n.bordes-left[data-v-746f853a]{\n  border-left: 2px solid;\n}\n.bordes-right[data-v-746f853a]{\n  border-right: 2px solid;\n}\n.bordes-bottom[data-v-746f853a]{\n  border-bottom: 2px solid;\n}\n.bordes-top-clarito[data-v-746f853a]{\n  border-top: 1px solid;\n}\n.bordes-header[data-v-746f853a]{\n  border: 2px solid;\n  border-bottom: 2px solid !important;\n}\n.bordes-bottom-clarito[data-v-746f853a]{\n  border-bottom:1px solid #e0e0e0;\n}\n.ceroPadding[data-v-746f853a]{\n  padding: 0px 0px!important;\n}\n.nombre-categoria[data-v-746f853a]{\n  background-color: #e1b618;\n  color: black !important;\n  font-size: 14px !important;\n}\n.sin-bordes-bottom-clarito[data-v-746f853a]{\n  border-bottom: 0px solid !important;\n}\n.cursor[data-v-746f853a]{\n  cursor: pointer!important;\n}\n.v-data-table__wrapper[data-v-746f853a] {\n  overflow-x: hidden !important;\n}\n.errorInput[data-v-746f853a]{\n  border: 1px solid red;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsAdminComponent.vue?vue&type=style&index=0&id=746f853a&scoped=true&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/componentes/TicketsAdminComponent.vue?vue&type=style&index=0&id=746f853a&scoped=true&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./TicketsAdminComponent.vue?vue&type=style&index=0&id=746f853a&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsAdminComponent.vue?vue&type=style&index=0&id=746f853a&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsAdminComponent.vue?vue&type=script&lang=ts&":
/*!***************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib??vue-loader-options!./resources/js/componentes/TicketsAdminComponent.vue?vue&type=script&lang=ts& ***!
  \***************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);







































































































































































































































































































































































































































































































/* harmony default export */ __webpack_exports__["default"] = ({
 props:{
    categorias: {
        type: Array,
        default: []
    },
    tickets: {
        type: Array,
        default: []
    }
  },
  components: {
    NotaComponent: () => __webpack_require__.e(/*! import() */ 5).then(__webpack_require__.bind(null, /*! ./NotaComponent.vue */ "./resources/js/componentes/NotaComponent.vue")),
    ReservaComponent: () => __webpack_require__.e(/*! import() */ 7).then(__webpack_require__.bind(null, /*! ./ReservaComponent.vue */ "./resources/js/componentes/ReservaComponent.vue")),
    PlusComisionComponent: () => __webpack_require__.e(/*! import() */ 6).then(__webpack_require__.bind(null, /*! ./PlusComisionComponent.vue */ "./resources/js/componentes/PlusComisionComponent.vue")),
  },
  data: () => ({
   
    opcionesPermitidas: [
      {
        nombre: "Si",
        valor: "1"
      },
      {
        nombre: "No",
        valor: "0"
      }
    ],
    opcionesFormato: ['Tarjeta', 'Papel', 'Móvil'],
    evento: null,
    editarTicket: null,
    siEdicion: null,
    siEdicion2: null,
    mostrarTicket: false,
    agregarTicketStock: null,
    permitirDisponibilidad: null,
    validarFormato: false,
    validarJuntas: false,
    validarSector: false,
    validarPrecioUd: false,
    validarGrupo: false,
    formatos: ['Papel', 'Tarjeta', 'Móvil'],
    nuevaReservacion: {
      ticket: null, 
      usuario_id: null,
      evento: null,
      precioFinal: null
    },
    reservarTicket: false,
    usuarioReservacion: null,
    existeNota: false
  }),
  
  methods: {
    arrGrupo(idCategoria){
      this.$emit('arrGrupo', idCategoria)
    },

    mostrarTicketEdicion(grupo){

      if(this.siEdicion == grupo.id){
        this.siEdicion = null
      }
      else{
        this.siEdicion = grupo.id
      }

      this.$emit('deStockAdisponible', grupo)

    },

    verificarSiHayTicketEnStock(grupo){
      this.permitirDisponibilidad = null

      this.$parent.ticketsDisponibles.filter(ticket => {
        if(ticket.grupo_id == grupo.id && ticket.grupo.categoria_evento_id == this.$parent.catId){
            ticket.grupo.permitir_ticket_max =  grupo.permitir_ticket_max
        }

      })

      this.$emit('deStockAdisponible', grupo)
      this.$emit('actualizandoTicketMax', grupo)

    },

    reservar(respuesta){

      this.nuevaReservacion.ticket = respuesta.ticket
      this.nuevaReservacion.usuario_id = respuesta.usuario
      this.nuevaReservacion.evento = this.$parent.evento.id
      this.nuevaReservacion.precioFinal = this.obtenerPrecioFinal(respuesta.ticket)

      this.$emit('apiRestReservacionTicket', this.nuevaReservacion)

    },

    aStock(ticket){
      
      if(ticket.estado == 1){
        let index = this.$parent.ticketsDisponibles.map(item => item.id).indexOf(ticket.id)
        this.$parent.ticketsDisponibles.splice(index, 1);
      }else if(ticket.estado == 3){
        let index = this.$parent.ticketsReservados.map(item => item.id).indexOf(ticket.id)
        this.$parent.ticketsReservados.splice(index, 1);
      }

      this.$emit('apiRestActualizarTicket', ticket, 2)

    },

    eliminarTickets(ticket) {
      var opcion = confirm("¿Estás seguro de querer eliminar?");

      if (opcion == true) {
        
        this.$emit("apiRestEliminar", ticket)

      } 
    },

    guardarNuevoTicket(grupo){
     
      this.$parent.nuevoTicketStock.grupo_id = grupo.id

      this.validarTicket()

      if(!this.validarSector && !this.validarFormato && !this.validarJuntas && !this.validarPrecioUd){
        // this.nuevoTicketStock.precioFinal = grupo.numero * parseInt(this.nuevoTicketStock.precioUd)
        
        this.$emit('apiRestCrearTicketStock', this.$parent.nuevoTicketStock)
        this.agregarTicketStock = null
        
      }
      

    },

    validarTicket(){
      if(!this.$parent.nuevoTicketStock.formato)
        this.validarFormato = true
      else
        this.validarFormato = false

      if(!this.$parent.nuevoTicketStock.juntas)  
        this.validarJuntas = true
      else
        this.validarJuntas = false

      if(!this.$parent.nuevoTicketStock.precioUd)  
        this.validarPrecioUd = true
      else
        this.validarPrecioUd = false

      if(!this.$parent.nuevoTicketStock.sector)  
        this.validarSector = true
      else
        this.validarSector = false
        
    },

    guardarTicket(ticket){

      if(!ticket.grupo.numero)
        this.validarGrupo = true
      else 
        this.validarGrupo = false  

      if(!ticket.formato)
        this.validarFormato = true
      else
        this.validarFormato = false

      if(!ticket.juntas)  
        this.validarJuntas = true
      else
        this.validarJuntas = false

      if(!ticket.precioUd)  
        this.validarPrecioUd = true
      else
        this.validarPrecioUd = false

      if(!ticket.sector)  
        this.validarSector = true
      else
        this.validarSector = false

      
      if(!this.validarGrupo && !this.validarSector && !this.validarFormato && !this.validarJuntas && !this.validarPrecioUd){
        
        this.$emit('apiRestEditarTicket', ticket)
        this.editarTicket = null
      }

    },

    enviarReservacion(ticket){
      let reservation = this.$parent.reservasTodas.find(function(value, index) {
          if(value.ticket_id == ticket.id)
            return value
      })

      return reservation

    },

    obtenerCategoriaInicial(){
      
      this.$parent.catId = this.categorias ? this.categorias[0].id : null
      // this.obtenerTicketsFiltrados(this.$parent.catId)

    },
    obtenerPrecioFinal(ticket){
       let numero = Number(ticket.grupo.numero)
       let precioUd = Number(ticket.precioUd)

       return numero * precioUd
    },

    guardarNota(tickeId, nota){
      this.$emit('apiGuardarNota', tickeId, nota)
    },

    mostrarSoloSiNoHayTicketReservado(grupo, categoria){

      let ticketsReservados = this.$parent.ticketsReservados.filter(ticket => ticket.grupo_id == grupo.id && this.$parent.catId == categoria.id)
      // console.log(ticketsReservados)
      if(ticketsReservados.length == 0){
        return true;
      }else{
        return false;
      }

    },

    eliminarGrupo(grupoId){
      var opcion = confirm("¿Estás seguro de querer eliminar?");

      if (opcion == true) {
        this.$emit('apiEliminarGrupo', grupoId)
      } 
    }

  }
});


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsAdminComponent.vue?vue&type=template&id=746f853a&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/componentes/TicketsAdminComponent.vue?vue&type=template&id=746f853a&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("v-simple-table", {
    attrs: { dense: "" },
    scopedSlots: _vm._u([
      {
        key: "default",
        fn: function() {
          return [
            _vm._l(_vm.categorias, function(categoria) {
              return _c(
                "thead",
                { key: categoria.nombre },
                [
                  _c("tr", [
                    _c("th", {
                      staticStyle: { "border-bottom": "0px" },
                      attrs: { colspan: "3" }
                    }),
                    _vm._v(" "),
                    _c(
                      "th",
                      {
                        staticClass:
                          "text-center cursor bordes-header nombre-categoria text-uppercase",
                        style: "background-color: " + categoria.color,
                        attrs: { colspan: "6" },
                        on: {
                          click: function($event) {
                            return _vm.arrGrupo(categoria.id)
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n            " +
                            _vm._s(categoria.nombre) +
                            "\n          "
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("th", {
                      staticStyle: { "border-bottom": "0px" },
                      attrs: { colspan: "3" }
                    })
                  ]),
                  _vm._v(" "),
                  _vm.$parent.catId == categoria.id
                    ? _c("tr", [
                        _c("th", {
                          staticStyle: { "border-bottom": "0px" },
                          attrs: { colspan: "3" }
                        }),
                        _vm._v(" "),
                        _c("th", { staticClass: "text-center bordes-header" }, [
                          _c("div", { staticClass: "mt-1" }, [
                            _vm._v("N° Tickets")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("th", { staticClass: "text-center bordes-header" }, [
                          _c("div", { staticClass: "mt-1" }, [_vm._v("Sector")])
                        ]),
                        _vm._v(" "),
                        _c("th", { staticClass: "text-center bordes-header" }, [
                          _c("div", { staticClass: "mt-1" }, [_vm._v("Juntas")])
                        ]),
                        _vm._v(" "),
                        _c("th", { staticClass: "text-center bordes-header" }, [
                          _c("div", { staticClass: "mt-1" }, [
                            _vm._v("Formato")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("th", { staticClass: "text-center bordes-header" }, [
                          _c("div", { staticClass: "mt-1" }, [
                            _vm._v("Precio / ud")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("th", { staticClass: "text-center bordes-header" }, [
                          _c("div", { staticClass: "mt-1" }, [
                            _vm._v("Precio Total")
                          ])
                        ]),
                        _c("th", {
                          staticStyle: { "border-bottom": "0px" },
                          attrs: { colspan: "3" }
                        })
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm._l(_vm.$parent.gruposPorCategoria, function(grupo) {
                    return [
                      _vm._l(_vm.$parent.ticketsDisponibles, function(
                        item,
                        index
                      ) {
                        return item.grupo_id == grupo.id &&
                          _vm.$parent.catId == categoria.id
                          ? _c("tr", [
                              _c(
                                "th",
                                {
                                  class: "text-center ceroPadding",
                                  staticStyle: {
                                    padding: "5px 6px 0 0 !important",
                                    "border-bottom": "0px"
                                  }
                                },
                                [
                                  _c("plus-comision-component", {
                                    attrs: {
                                      ticket: item,
                                      usuarios: _vm.$parent.usuarios
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "th",
                                {
                                  class: "text-center ceroPadding",
                                  staticStyle: {
                                    padding: "5px!important",
                                    "border-bottom": "0px"
                                  }
                                },
                                [
                                  _c("nota-component", {
                                    attrs: {
                                      ticketId: item.id,
                                      nota: item.descripcion,
                                      reservacion: null,
                                      usuario: null,
                                      precioNeto: null
                                    },
                                    on: { guardarNota: _vm.guardarNota }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class: "text-center ceroPadding",
                                      staticStyle: { "border-bottom": "0px" },
                                      on: {
                                        click: function($event) {
                                          _vm.editarTicket = item.id
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "v-icon",
                                        { attrs: { color: "black" } },
                                        [_vm._v("mdi-pencil")]
                                      )
                                    ],
                                    1
                                  )
                                : _c(
                                    "th",
                                    {
                                      class: "text-center ceroPadding",
                                      staticStyle: { "border-bottom": "0px" }
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          on: {
                                            click: function($event) {
                                              return _vm.guardarTicket(item)
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "v-icon",
                                            { attrs: { color: "black" } },
                                            [_vm._v("mdi-checkbox-marked")]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        {
                                          on: {
                                            click: function($event) {
                                              _vm.editarTicket = null
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "v-icon",
                                            { attrs: { color: "black" } },
                                            [_vm._v("mdi-close-box")]
                                          )
                                        ],
                                        1
                                      )
                                    ]
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center cursor bordes-left colorSegunEstado" +
                                        item.estado,
                                      on: {
                                        click: function($event) {
                                          return _vm.mostrarTicketEdicion(grupo)
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(grupo.numero) +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center cursor bordes-left colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: {
                                          error: _vm.validarGrupo,
                                          label: "Número",
                                          type: "number"
                                        },
                                        model: {
                                          value: item.grupo.numero,
                                          callback: function($$v) {
                                            _vm.$set(item.grupo, "numero", $$v)
                                          },
                                          expression: "item.grupo.numero"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(item.sector) +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: {
                                          error: _vm.validarSector,
                                          label: "Sector",
                                          required: ""
                                        },
                                        model: {
                                          value: item.sector,
                                          callback: function($$v) {
                                            _vm.$set(item, "sector", $$v)
                                          },
                                          expression: "item.sector"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(item.juntas) +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: {
                                          error: _vm.validarJuntas,
                                          label: "Juntas",
                                          required: ""
                                        },
                                        model: {
                                          value: item.juntas,
                                          callback: function($$v) {
                                            _vm.$set(item, "juntas", $$v)
                                          },
                                          expression: "item.juntas"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(item.formato) +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-select", {
                                        attrs: {
                                          items: _vm.opcionesFormato,
                                          label:
                                            "N° máximo tickets disponibles:",
                                          error: _vm.validarFormato,
                                          required: ""
                                        },
                                        model: {
                                          value: item.formato,
                                          callback: function($$v) {
                                            _vm.$set(item, "formato", $$v)
                                          },
                                          expression: "item.formato"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(item.precioUd + " €") +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: {
                                          type: "number",
                                          label: "Precio Ud",
                                          error: _vm.validarPrecioUd,
                                          required: ""
                                        },
                                        model: {
                                          value: item.precioUd,
                                          callback: function($$v) {
                                            _vm.$set(item, "precioUd", $$v)
                                          },
                                          expression: "item.precioUd"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center bordes-right colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(
                                            _vm.obtenerPrecioFinal(item) + " €"
                                          ) +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center bordes-right colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: {
                                          label: "Precio total",
                                          disabled: ""
                                        },
                                        model: {
                                          value: item.precioFinal,
                                          callback: function($$v) {
                                            _vm.$set(item, "precioFinal", $$v)
                                          },
                                          expression: "item.precioFinal"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _c(
                                "th",
                                {
                                  class: "text-center ceroPadding",
                                  staticStyle: { "border-bottom": "0px" },
                                  attrs: { title: "Eliminar ticket" },
                                  on: {
                                    click: function($event) {
                                      return _vm.eliminarTickets(item)
                                    }
                                  }
                                },
                                [
                                  _c("v-icon", { attrs: { color: "red" } }, [
                                    _vm._v("mdi-close-circle")
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "th",
                                {
                                  class: "text-center ceroPadding",
                                  staticStyle: {
                                    padding: "5px!important",
                                    "border-bottom": "0px"
                                  },
                                  attrs: { title: "Reservar ticket" }
                                },
                                [
                                  !_vm.$parent.reservando ||
                                  item.id != _vm.$parent.reservando
                                    ? _c("reserva-component", {
                                        attrs: {
                                          grupo: grupo,
                                          ticket: item,
                                          usuarios: _vm.$parent.usuarios
                                        },
                                        on: { click: _vm.reservar }
                                      })
                                    : _vm.$parent.reservando &&
                                      item.id == _vm.$parent.reservando
                                    ? _c("v-progress-circular", {
                                        attrs: {
                                          indeterminate: "",
                                          color: "black",
                                          size: 15,
                                          width: 1
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "th",
                                {
                                  class: "text-center ceroPadding",
                                  staticStyle: { "border-bottom": "0px" },
                                  attrs: { title: "Mover a stock" },
                                  on: {
                                    click: function($event) {
                                      return _vm.aStock(item)
                                    }
                                  }
                                },
                                [
                                  _c("v-icon", { attrs: { color: "black" } }, [
                                    _vm._v("mdi-briefcase")
                                  ])
                                ],
                                1
                              )
                            ])
                          : _vm._e()
                      }),
                      _vm._v(" "),
                      _vm._l(_vm.$parent.gruposCero, function(item) {
                        return item == grupo.id &&
                          _vm.$parent.catId == categoria.id
                          ? _c("tr", [
                              _c("th", {
                                staticStyle: { "border-bottom": "0px" },
                                attrs: { colspan: "3" }
                              }),
                              _vm._v(" "),
                              _c(
                                "th",
                                {
                                  class: "text-center cursor bordes-left",
                                  on: {
                                    click: function($event) {
                                      return _vm.mostrarTicketEdicion(grupo)
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "Grupo " +
                                      _vm._s(grupo.numero) +
                                      "\n               \n              "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("th", {
                                class: "bordes-right",
                                attrs: { colspan: "5" }
                              }),
                              _vm._v(" "),
                              _c(
                                "th",
                                {
                                  staticStyle: { "border-bottom": "0px" },
                                  attrs: { colspan: "3" }
                                },
                                [
                                  _vm.mostrarSoloSiNoHayTicketReservado(
                                    grupo,
                                    categoria
                                  )
                                    ? _c(
                                        "v-icon",
                                        {
                                          attrs: { small: "" },
                                          on: {
                                            click: function($event) {
                                              return _vm.eliminarGrupo(item)
                                            }
                                          }
                                        },
                                        [
                                          _vm._v(
                                            "\n                  mdi-delete\n                "
                                          )
                                        ]
                                      )
                                    : _vm._e()
                                ],
                                1
                              )
                            ])
                          : _vm._e()
                      }),
                      _vm._v(" "),
                      _vm.siEdicion == grupo.id &&
                      _vm.$parent.catId == categoria.id &&
                      _vm.agregarTicketStock != grupo.id
                        ? _c("tr", [
                            _c("th", {
                              staticClass: "sin-bordes-bottom-clarito",
                              staticStyle: { "border-bottom": "0px" },
                              attrs: { colspan: "3" }
                            }),
                            _vm._v(" "),
                            _c(
                              "th",
                              {
                                staticClass: "bordes-left bordes-right",
                                attrs: { colspan: "6" }
                              },
                              [
                                _vm.permitirDisponibilidad == grupo.id
                                  ? _c(
                                      "div",
                                      [
                                        _c(
                                          "v-row",
                                          [
                                            _c(
                                              "v-col",
                                              { attrs: { cols: "5" } },
                                              [
                                                _c("v-select", {
                                                  attrs: {
                                                    items:
                                                      _vm.opcionesPermitidas,
                                                    "item-text": "nombre",
                                                    "item-value": "valor",
                                                    label:
                                                      "N° máximo tickets disponibles:"
                                                  },
                                                  model: {
                                                    value:
                                                      grupo.permitir_ticket_max,
                                                    callback: function($$v) {
                                                      _vm.$set(
                                                        grupo,
                                                        "permitir_ticket_max",
                                                        $$v
                                                      )
                                                    },
                                                    expression:
                                                      "grupo.permitir_ticket_max"
                                                  }
                                                })
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "v-col",
                                              { attrs: { cols: "5" } },
                                              [
                                                _c("v-text-field", {
                                                  attrs: {
                                                    label:
                                                      "Cantidad max disponible"
                                                  },
                                                  model: {
                                                    value:
                                                      grupo.cantidad_max_disponible,
                                                    callback: function($$v) {
                                                      _vm.$set(
                                                        grupo,
                                                        "cantidad_max_disponible",
                                                        $$v
                                                      )
                                                    },
                                                    expression:
                                                      "grupo.cantidad_max_disponible"
                                                  }
                                                })
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "v-col",
                                              {
                                                staticClass: "pt-8",
                                                attrs: { cols: "2" }
                                              },
                                              [
                                                _c(
                                                  "span",
                                                  {
                                                    on: {
                                                      click: function($event) {
                                                        _vm.permitirDisponibilidad = null
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "v-icon",
                                                      {
                                                        attrs: {
                                                          color: "black"
                                                        }
                                                      },
                                                      [_vm._v("mdi-close-box")]
                                                    )
                                                  ],
                                                  1
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "span",
                                                  {
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.verificarSiHayTicketEnStock(
                                                          grupo
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "v-icon",
                                                      {
                                                        attrs: {
                                                          color: "black"
                                                        }
                                                      },
                                                      [
                                                        _vm._v(
                                                          "mdi-checkbox-marked"
                                                        )
                                                      ]
                                                    )
                                                  ],
                                                  1
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        )
                                      ],
                                      1
                                    )
                                  : _c(
                                      "div",
                                      [
                                        _c(
                                          "v-row",
                                          [
                                            _c(
                                              "v-col",
                                              { attrs: { cols: "5" } },
                                              [
                                                _vm._v(
                                                  "\n                    N° máximo tickets disponibles: \n                    "
                                                ),
                                                grupo.permitir_ticket_max == "1"
                                                  ? _c("span", [
                                                      _vm._v(
                                                        "Sí (" +
                                                          _vm._s(
                                                            grupo.cantidad_max_disponible
                                                          ) +
                                                          ") "
                                                      )
                                                    ])
                                                  : grupo.permitir_ticket_max ==
                                                    "0"
                                                  ? _c("span", [_vm._v("No")])
                                                  : _vm._e(),
                                                _vm._v(" "),
                                                _c(
                                                  "span",
                                                  {
                                                    on: {
                                                      click: function($event) {
                                                        _vm.permitirDisponibilidad =
                                                          grupo.id
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "v-icon",
                                                      {
                                                        attrs: {
                                                          color: "black"
                                                        }
                                                      },
                                                      [_vm._v("mdi-pencil")]
                                                    )
                                                  ],
                                                  1
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "v-col",
                                              { attrs: { cols: "7" } },
                                              [
                                                _c(
                                                  "div",
                                                  {
                                                    on: {
                                                      click: function($event) {
                                                        _vm.agregarTicketStock =
                                                          grupo.id
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "v-icon",
                                                      {
                                                        attrs: {
                                                          color: "black"
                                                        }
                                                      },
                                                      [_vm._v("mdi-plus-thick")]
                                                    ),
                                                    _vm._v(
                                                      " Añadir nuevos tickets al stock\n                    "
                                                    )
                                                  ],
                                                  1
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        )
                                      ],
                                      1
                                    )
                              ]
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.siEdicion == grupo.id &&
                      _vm.$parent.catId == categoria.id &&
                      _vm.agregarTicketStock == grupo.id
                        ? _c("tr", [
                            _c("th", {
                              staticStyle: { "border-bottom": "0px" },
                              attrs: { colspan: "3" }
                            }),
                            _vm._v(" "),
                            _c(
                              "th",
                              {
                                staticClass: "bordes-left bordes-right",
                                attrs: { colspan: "6" }
                              },
                              [
                                _c(
                                  "div",
                                  [
                                    _c(
                                      "v-row",
                                      [
                                        _c(
                                          "v-col",
                                          { attrs: { cols: "2" } },
                                          [
                                            _c("v-text-field", {
                                              attrs: {
                                                label: "N° Tickets",
                                                type: "number",
                                                disabled: ""
                                              },
                                              model: {
                                                value: grupo.numero,
                                                callback: function($$v) {
                                                  _vm.$set(grupo, "numero", $$v)
                                                },
                                                expression: "grupo.numero"
                                              }
                                            })
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "v-col",
                                          { attrs: { cols: "2" } },
                                          [
                                            _c("v-text-field", {
                                              attrs: {
                                                label: "Sector",
                                                error: _vm.validarSector
                                              },
                                              model: {
                                                value:
                                                  _vm.$parent.nuevoTicketStock
                                                    .sector,
                                                callback: function($$v) {
                                                  _vm.$set(
                                                    _vm.$parent
                                                      .nuevoTicketStock,
                                                    "sector",
                                                    $$v
                                                  )
                                                },
                                                expression:
                                                  "$parent.nuevoTicketStock.sector"
                                              }
                                            })
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "v-col",
                                          { attrs: { cols: "2" } },
                                          [
                                            _c("v-text-field", {
                                              attrs: {
                                                label: "Junta",
                                                error: _vm.validarJuntas
                                              },
                                              model: {
                                                value:
                                                  _vm.$parent.nuevoTicketStock
                                                    .juntas,
                                                callback: function($$v) {
                                                  _vm.$set(
                                                    _vm.$parent
                                                      .nuevoTicketStock,
                                                    "juntas",
                                                    $$v
                                                  )
                                                },
                                                expression:
                                                  "$parent.nuevoTicketStock.juntas"
                                              }
                                            })
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "v-col",
                                          { attrs: { cols: "2" } },
                                          [
                                            _c("v-select", {
                                              attrs: {
                                                items: _vm.formatos,
                                                label: "Formato",
                                                error: _vm.validarFormato
                                              },
                                              model: {
                                                value:
                                                  _vm.$parent.nuevoTicketStock
                                                    .formato,
                                                callback: function($$v) {
                                                  _vm.$set(
                                                    _vm.$parent
                                                      .nuevoTicketStock,
                                                    "formato",
                                                    $$v
                                                  )
                                                },
                                                expression:
                                                  "$parent.nuevoTicketStock.formato"
                                              }
                                            })
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "v-col",
                                          { attrs: { cols: "2" } },
                                          [
                                            _c("v-text-field", {
                                              attrs: {
                                                label: "Precio/Ud",
                                                type: "number",
                                                error: _vm.validarPrecioUd
                                              },
                                              model: {
                                                value:
                                                  _vm.$parent.nuevoTicketStock
                                                    .precioUd,
                                                callback: function($$v) {
                                                  _vm.$set(
                                                    _vm.$parent
                                                      .nuevoTicketStock,
                                                    "precioUd",
                                                    $$v
                                                  )
                                                },
                                                expression:
                                                  "$parent.nuevoTicketStock.precioUd"
                                              }
                                            })
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "v-col",
                                          {
                                            staticClass: "pt-8",
                                            attrs: { cols: "2" }
                                          },
                                          [
                                            _c(
                                              "span",
                                              {
                                                on: {
                                                  click: function($event) {
                                                    _vm.agregarTicketStock = null
                                                  }
                                                }
                                              },
                                              [
                                                _c(
                                                  "v-icon",
                                                  { attrs: { color: "black" } },
                                                  [_vm._v("mdi-close-box")]
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "span",
                                              {
                                                on: {
                                                  click: function($event) {
                                                    return _vm.guardarNuevoTicket(
                                                      grupo
                                                    )
                                                  }
                                                }
                                              },
                                              [
                                                _c(
                                                  "v-icon",
                                                  { attrs: { color: "black" } },
                                                  [
                                                    _vm._v(
                                                      "mdi-checkbox-marked"
                                                    )
                                                  ]
                                                )
                                              ],
                                              1
                                            )
                                          ]
                                        )
                                      ],
                                      1
                                    )
                                  ],
                                  1
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c("th", {
                              staticStyle: { "border-bottom": "0px" },
                              attrs: { colspan: "3" }
                            })
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm._l(_vm.$parent.ticketsStock, function(item, index) {
                        return item.grupo_id == grupo.id &&
                          _vm.siEdicion == grupo.id &&
                          _vm.$parent.catId == categoria.id
                          ? _c("tr", [
                              _c(
                                "th",
                                {
                                  class: "text-center ceroPadding",
                                  staticStyle: {
                                    padding: "5px 6px 0 0 !important",
                                    "border-bottom": "0px"
                                  }
                                },
                                [
                                  _c("plus-comision-component", {
                                    attrs: {
                                      ticket: item,
                                      usuarios: _vm.$parent.usuarios
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "th",
                                {
                                  class: "text-center ceroPadding",
                                  staticStyle: {
                                    padding: "5px!important",
                                    "border-bottom": "0px"
                                  }
                                },
                                [
                                  _c("nota-component", {
                                    attrs: {
                                      ticketId: item.id,
                                      nota: item.descripcion,
                                      reservacion: null,
                                      usuario: null,
                                      precioNeto: null
                                    },
                                    on: { guardarNota: _vm.guardarNota }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class: "text-center ceroPadding",
                                      staticStyle: { "border-bottom": "0px" },
                                      on: {
                                        click: function($event) {
                                          _vm.editarTicket = item.id
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "v-icon",
                                        { attrs: { color: "black" } },
                                        [_vm._v("mdi-pencil")]
                                      )
                                    ],
                                    1
                                  )
                                : _c(
                                    "th",
                                    {
                                      class: "text-center ceroPadding",
                                      staticStyle: { "border-bottom": "0px" }
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          on: {
                                            click: function($event) {
                                              return _vm.guardarTicket(item)
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "v-icon",
                                            { attrs: { color: "black" } },
                                            [_vm._v("mdi-checkbox-marked")]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        {
                                          on: {
                                            click: function($event) {
                                              _vm.editarTicket = null
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "v-icon",
                                            { attrs: { color: "black" } },
                                            [_vm._v("mdi-close-box")]
                                          )
                                        ],
                                        1
                                      )
                                    ]
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center bordes-left colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(grupo.numero) +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center bordes-left colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: { disabled: "" },
                                        model: {
                                          value: grupo.numero,
                                          callback: function($$v) {
                                            _vm.$set(grupo, "numero", $$v)
                                          },
                                          expression: "grupo.numero"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(item.sector) +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: { required: "" },
                                        model: {
                                          value: item.sector,
                                          callback: function($$v) {
                                            _vm.$set(item, "sector", $$v)
                                          },
                                          expression: "item.sector"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(item.juntas) +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: { required: "" },
                                        model: {
                                          value: item.juntas,
                                          callback: function($$v) {
                                            _vm.$set(item, "juntas", $$v)
                                          },
                                          expression: "item.juntas"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(item.formato) +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: { required: "" },
                                        model: {
                                          value: item.formato,
                                          callback: function($$v) {
                                            _vm.$set(item, "formato", $$v)
                                          },
                                          expression: "item.formato"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(item.precioUd + " €") +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: { required: "" },
                                        model: {
                                          value: item.precioUd,
                                          callback: function($$v) {
                                            _vm.$set(item, "precioUd", $$v)
                                          },
                                          expression: "item.precioUd"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center bordes-right colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(
                                            _vm.obtenerPrecioFinal(item) + " €"
                                          ) +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center bordes-right colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: { disabled: "" },
                                        model: {
                                          value: item.precioFinal,
                                          callback: function($$v) {
                                            _vm.$set(item, "precioFinal", $$v)
                                          },
                                          expression: "item.precioFinal"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _c(
                                "th",
                                {
                                  class: "text-center ceroPadding",
                                  staticStyle: { "border-bottom": "0px" },
                                  attrs: { title: "Eliminar ticket" },
                                  on: {
                                    click: function($event) {
                                      return _vm.eliminarTickets(item)
                                    }
                                  }
                                },
                                [
                                  _c("v-icon", { attrs: { color: "red" } }, [
                                    _vm._v("mdi-close-circle\n                ")
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "th",
                                {
                                  class: "text-center ceroPadding",
                                  staticStyle: {
                                    padding: "5px!important",
                                    "border-bottom": "0px"
                                  },
                                  attrs: { title: "Reservar ticket" }
                                },
                                [
                                  !_vm.$parent.reservando ||
                                  item.id != _vm.$parent.reservando
                                    ? _c("reserva-component", {
                                        attrs: {
                                          grupo: grupo,
                                          ticket: item,
                                          usuarios: _vm.$parent.usuarios
                                        },
                                        on: { click: _vm.reservar }
                                      })
                                    : _vm.$parent.reservando &&
                                      item.id == _vm.$parent.reservando
                                    ? _c("v-progress-circular", {
                                        attrs: {
                                          indeterminate: "",
                                          color: "black",
                                          size: 15,
                                          width: 1
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("th", {
                                staticStyle: { "border-bottom": "0px" }
                              })
                            ])
                          : _vm._e()
                      }),
                      _vm._v(" "),
                      _vm._l(_vm.$parent.ticketsReservados, function(
                        item,
                        index
                      ) {
                        return item.grupo_id == grupo.id &&
                          _vm.siEdicion == grupo.id &&
                          _vm.$parent.catId == categoria.id
                          ? _c("tr", [
                              _c(
                                "th",
                                {
                                  class: "text-center ceroPadding",
                                  staticStyle: {
                                    padding: "5px 6px 0 0 !important",
                                    "border-bottom": "0px"
                                  }
                                },
                                [
                                  _c("plus-comision-component", {
                                    attrs: {
                                      ticket: item,
                                      usuarios: _vm.$parent.usuarios
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "th",
                                {
                                  class: "text-center ceroPadding",
                                  staticStyle: {
                                    padding: "5px!important",
                                    "border-bottom": "0px"
                                  }
                                },
                                [
                                  _c("nota-component", {
                                    attrs: {
                                      ticketId: item.id,
                                      nota: item.descripcion,
                                      reservacion: _vm.enviarReservacion(item),
                                      usuario: null,
                                      precioNeto: parseFloat(item.precioUd)
                                    },
                                    on: { guardarNota: _vm.guardarNota }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class: "text-center ceroPadding",
                                      staticStyle: { "border-bottom": "0px" },
                                      on: {
                                        click: function($event) {
                                          _vm.editarTicket = item.id
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "v-icon",
                                        { attrs: { color: "black" } },
                                        [_vm._v("mdi-pencil")]
                                      )
                                    ],
                                    1
                                  )
                                : _c(
                                    "th",
                                    {
                                      class: "text-center ceroPadding",
                                      staticStyle: { "border-bottom": "0px" }
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          on: {
                                            click: function($event) {
                                              return _vm.guardarTicket(item)
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "v-icon",
                                            { attrs: { color: "black" } },
                                            [_vm._v("mdi-checkbox-marked")]
                                          )
                                        ],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        {
                                          on: {
                                            click: function($event) {
                                              _vm.editarTicket = null
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "v-icon",
                                            { attrs: { color: "black" } },
                                            [_vm._v("mdi-close-box")]
                                          )
                                        ],
                                        1
                                      )
                                    ]
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center bordes-left colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(item.grupo.numero) +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center bordes-left colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: { disabled: "" },
                                        model: {
                                          value: item.grupo.numero,
                                          callback: function($$v) {
                                            _vm.$set(item.grupo, "numero", $$v)
                                          },
                                          expression: "item.grupo.numero"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(item.sector) +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: { required: "" },
                                        model: {
                                          value: item.sector,
                                          callback: function($$v) {
                                            _vm.$set(item, "sector", $$v)
                                          },
                                          expression: "item.sector"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(item.juntas) +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: { required: "" },
                                        model: {
                                          value: item.juntas,
                                          callback: function($$v) {
                                            _vm.$set(item, "juntas", $$v)
                                          },
                                          expression: "item.juntas"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(item.formato) +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: { required: "" },
                                        model: {
                                          value: item.formato,
                                          callback: function($$v) {
                                            _vm.$set(item, "formato", $$v)
                                          },
                                          expression: "item.formato"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(item.precioUd + " €") +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: { required: "" },
                                        model: {
                                          value: item.precioUd,
                                          callback: function($$v) {
                                            _vm.$set(item, "precioUd", $$v)
                                          },
                                          expression: "item.precioUd"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _vm.editarTicket != item.id
                                ? _c(
                                    "th",
                                    {
                                      class:
                                        "text-center bordes-right colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _vm._v(
                                        "\n              " +
                                          _vm._s(
                                            _vm.obtenerPrecioFinal(item) + " €"
                                          ) +
                                          "\n            "
                                      )
                                    ]
                                  )
                                : _c(
                                    "th",
                                    {
                                      class:
                                        "text-center bordes-right colorSegunEstado" +
                                        item.estado
                                    },
                                    [
                                      _c("v-text-field", {
                                        attrs: { disabled: "" },
                                        model: {
                                          value: item.precioFinal,
                                          callback: function($$v) {
                                            _vm.$set(item, "precioFinal", $$v)
                                          },
                                          expression: "item.precioFinal"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                              _vm._v(" "),
                              _c(
                                "th",
                                {
                                  class: "text-center ceroPadding",
                                  staticStyle: { "border-bottom": "0px" },
                                  attrs: { title: "Eliminar ticket" },
                                  on: {
                                    click: function($event) {
                                      return _vm.eliminarTickets(item)
                                    }
                                  }
                                },
                                [
                                  _c("v-icon", { attrs: { color: "red" } }, [
                                    _vm._v("mdi-close-circle")
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("th", {
                                staticStyle: { "border-bottom": "0px" }
                              }),
                              _vm._v(" "),
                              _c(
                                "th",
                                {
                                  class: "text-center ceroPadding",
                                  staticStyle: { "border-bottom": "0px" },
                                  attrs: { title: "Mover a stock" },
                                  on: {
                                    click: function($event) {
                                      return _vm.aStock(item)
                                    }
                                  }
                                },
                                [
                                  _c("v-icon", { attrs: { color: "black" } }, [
                                    _vm._v("mdi-briefcase")
                                  ])
                                ],
                                1
                              )
                            ])
                          : _vm._e()
                      })
                    ]
                  })
                ],
                2
              )
            }),
            _vm._v(" "),
            _c("thead", [
              _c("tr", [
                _c("th", {
                  staticStyle: { "border-bottom": "0px" },
                  attrs: { colspan: "3" }
                }),
                _vm._v(" "),
                _c("th", {
                  staticStyle: {
                    "border-top": "2px solid",
                    "border-bottom": "0px"
                  },
                  attrs: { colspan: "6" }
                })
              ])
            ])
          ]
        },
        proxy: true
      }
    ])
  })
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/componentes/TicketsAdminComponent.vue":
/*!************************************************************!*\
  !*** ./resources/js/componentes/TicketsAdminComponent.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TicketsAdminComponent_vue_vue_type_template_id_746f853a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TicketsAdminComponent.vue?vue&type=template&id=746f853a&scoped=true& */ "./resources/js/componentes/TicketsAdminComponent.vue?vue&type=template&id=746f853a&scoped=true&");
/* harmony import */ var _TicketsAdminComponent_vue_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TicketsAdminComponent.vue?vue&type=script&lang=ts& */ "./resources/js/componentes/TicketsAdminComponent.vue?vue&type=script&lang=ts&");
/* empty/unused harmony star reexport *//* harmony import */ var _TicketsAdminComponent_vue_vue_type_style_index_0_id_746f853a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./TicketsAdminComponent.vue?vue&type=style&index=0&id=746f853a&scoped=true&lang=css& */ "./resources/js/componentes/TicketsAdminComponent.vue?vue&type=style&index=0&id=746f853a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _TicketsAdminComponent_vue_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TicketsAdminComponent_vue_vue_type_template_id_746f853a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TicketsAdminComponent_vue_vue_type_template_id_746f853a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "746f853a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/componentes/TicketsAdminComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/componentes/TicketsAdminComponent.vue?vue&type=script&lang=ts&":
/*!*************************************************************************************!*\
  !*** ./resources/js/componentes/TicketsAdminComponent.vue?vue&type=script&lang=ts& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsAdminComponent_vue_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib??vue-loader-options!./TicketsAdminComponent.vue?vue&type=script&lang=ts& */ "./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsAdminComponent.vue?vue&type=script&lang=ts&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsAdminComponent_vue_vue_type_script_lang_ts___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/componentes/TicketsAdminComponent.vue?vue&type=style&index=0&id=746f853a&scoped=true&lang=css&":
/*!*********************************************************************************************************************!*\
  !*** ./resources/js/componentes/TicketsAdminComponent.vue?vue&type=style&index=0&id=746f853a&scoped=true&lang=css& ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsAdminComponent_vue_vue_type_style_index_0_id_746f853a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./TicketsAdminComponent.vue?vue&type=style&index=0&id=746f853a&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsAdminComponent.vue?vue&type=style&index=0&id=746f853a&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsAdminComponent_vue_vue_type_style_index_0_id_746f853a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsAdminComponent_vue_vue_type_style_index_0_id_746f853a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsAdminComponent_vue_vue_type_style_index_0_id_746f853a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsAdminComponent_vue_vue_type_style_index_0_id_746f853a_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/componentes/TicketsAdminComponent.vue?vue&type=template&id=746f853a&scoped=true&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/componentes/TicketsAdminComponent.vue?vue&type=template&id=746f853a&scoped=true& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsAdminComponent_vue_vue_type_template_id_746f853a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./TicketsAdminComponent.vue?vue&type=template&id=746f853a&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/TicketsAdminComponent.vue?vue&type=template&id=746f853a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsAdminComponent_vue_vue_type_template_id_746f853a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TicketsAdminComponent_vue_vue_type_template_id_746f853a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);