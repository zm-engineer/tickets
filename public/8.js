(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[8],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/backend/historiales/ListadoAdminComponent.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/backend/historiales/ListadoAdminComponent.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

moment__WEBPACK_IMPORTED_MODULE_0___default.a.locale('es');
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      search: '',
      editedIndex: null,
      dialogDelete: false,
      headers: [{
        text: 'Evento',
        align: 'start',
        value: 'evento'
      }, {
        text: 'Fecha y hora',
        value: 'fecha',
        sortable: false
      }, {
        text: 'Cliente',
        value: 'cliente'
      }, {
        text: 'Usuario',
        value: 'usuario',
        sortable: false
      }, {
        text: 'Conserje',
        value: 'conserje',
        sortable: false
      }, {
        text: 'Fecha de reserva',
        value: 'created_at'
      }, {
        text: 'N° Tickets',
        value: 'numero',
        sortable: false
      }, {
        text: 'Categoría',
        value: 'categoria',
        sortable: false
      }, {
        text: 'Sector',
        value: 'sector',
        sortable: false
      }, {
        text: 'Juntas',
        value: 'juntas',
        sortable: false
      }, {
        text: 'Formato',
        value: 'formato',
        sortable: false
      }, {
        text: 'P. Neto',
        value: 'precioUd',
        sortable: false
      }, {
        text: 'P. Neto + %plus',
        value: 'plus',
        sortable: false
      }, {
        text: 'P. Neto + %plus + %comisión',
        value: 'comision',
        sortable: false
      }, {
        text: 'P. Total',
        value: 'total',
        sortable: false
      }, {
        text: 'Eliminar',
        value: 'eliminar',
        sortable: false
      }]
    };
  },
  props: {
    reservas: {
      type: Array,
      "default": []
    }
  },
  methods: {
    obtenerPrecioPlus: function obtenerPrecioPlus(reserva) {
      var dp = reserva.precioUd * reserva.plus;
      var pnp = parseFloat(reserva.precioUd) + parseFloat(dp);
      var resultado = pnp.toFixed(2);
      return parseInt(resultado);
    },
    obtenerPrecioComision: function obtenerPrecioComision(reserva) {
      var dc = parseFloat(this.obtenerPrecioPlus(reserva)) * reserva.comision;
      var totalUnitario = parseFloat(this.obtenerPrecioPlus(reserva)) + parseFloat(dc);
      var resultado = totalUnitario.toFixed(2);
      return parseInt(resultado);
    },
    obtenerPrecioTotal: function obtenerPrecioTotal(reserva) {
      var resultado = reserva.numero * this.obtenerPrecioComision(reserva);
      resultado = resultado.toFixed(2);
      return parseInt(resultado);
    },
    deleteItem: function deleteItem(item) {
      this.editedIndex = this.reservas.indexOf(item);
      this.dialogDelete = true;
    },
    deleteItemConfirm: function deleteItemConfirm() {
      var _this = this;

      var reserva = this.reservas[this.editedIndex];
      axios["delete"]('/api/reservacion/' + this.reservas[this.editedIndex].id, {
        headers: {
          Authorization: "Bearer " + this.$store.state.token
        }
      }).then(function (response) {
        _this.reservas.splice(_this.editedIndex, 1);

        _this.closeDelete();

        _this.$toastr.s("Reserva eliminada correctamente.");
      })["catch"](function (error) {
        console.log(error);

        _this.$toastr.e("Ocurrio un error al eliminar");
      });
    },
    closeDelete: function closeDelete() {
      this.dialogDelete = false;
      this.editedIndex = -1;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/backend/historiales/ListadoAdminComponent.vue?vue&type=template&id=c919d1c0&":
/*!***************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/backend/historiales/ListadoAdminComponent.vue?vue&type=template&id=c919d1c0& ***!
  \***************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    [
      _c(
        "v-card-title",
        [
          _c("v-text-field", {
            attrs: {
              "append-icon": "mdi-magnify",
              label: "Buscar",
              "single-line": "",
              "hide-details": ""
            },
            model: {
              value: _vm.search,
              callback: function($$v) {
                _vm.search = $$v
              },
              expression: "search"
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c("v-data-table", {
        attrs: {
          headers: _vm.headers,
          items: _vm.reservas,
          search: _vm.search,
          "no-results-text": "No se encontraron datos",
          "no-data-text": "Sin resultados",
          "footer-props": {
            "items-per-page-options": [5, 10, 15, 20],
            "items-per-page-text": ""
          },
          "items-per-page": 5
        },
        scopedSlots: _vm._u([
          {
            key: "item.fecha",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("span", [
                  _vm._v(
                    _vm._s(_vm._f("moment")(item.fecha, "D MMMM YYYY HH:mm:ss"))
                  )
                ])
              ]
            }
          },
          {
            key: "item.created_at",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("span", [
                  _vm._v(
                    _vm._s(
                      _vm._f("moment")(
                        item.created_at,
                        "add",
                        "1 hours ",
                        "D MMMM YYYY HH:mm:ss"
                      )
                    )
                  )
                ])
              ]
            }
          },
          {
            key: "item.plus",
            fn: function(ref) {
              var item = ref.item
              return [_c("span", [_vm._v(_vm._s(_vm.obtenerPrecioPlus(item)))])]
            }
          },
          {
            key: "item.comision",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("span", [_vm._v(_vm._s(_vm.obtenerPrecioComision(item)))])
              ]
            }
          },
          {
            key: "item.total",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("span", [
                  _vm._v(_vm._s(_vm.obtenerPrecioTotal(item) + " €"))
                ])
              ]
            }
          },
          {
            key: "item.eliminar",
            fn: function(ref) {
              var item = ref.item
              return [
                _c(
                  "v-icon",
                  {
                    attrs: { small: "" },
                    on: {
                      click: function($event) {
                        return _vm.deleteItem(item)
                      }
                    }
                  },
                  [_vm._v("\n        mdi-delete\n      ")]
                )
              ]
            }
          },
          {
            key: "bootom",
            fn: function(ref) {
              var pagination = ref.pagination
              var options = ref.options
              var updateOptions = ref.updateOptions
              return [
                _c("v-data-footer", {
                  attrs: {
                    pagination: pagination,
                    options: options,
                    "items-per-page-text": "$vuetify.dataTable.itemsPerPageText"
                  },
                  on: { "update:options": updateOptions }
                })
              ]
            }
          }
        ])
      }),
      _vm._v(" "),
      _c(
        "v-dialog",
        {
          attrs: { "max-width": "500px" },
          model: {
            value: _vm.dialogDelete,
            callback: function($$v) {
              _vm.dialogDelete = $$v
            },
            expression: "dialogDelete"
          }
        },
        [
          _c(
            "v-card",
            [
              _c("v-card-title", { staticClass: "text-h5" }, [
                _vm._v("¿Estás seguro de eliminar?")
              ]),
              _vm._v(" "),
              _c(
                "v-card-actions",
                [
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "blue darken-1", text: "" },
                      on: { click: _vm.closeDelete }
                    },
                    [_vm._v("Cancelar")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "blue darken-1", text: "" },
                      on: { click: _vm.deleteItemConfirm }
                    },
                    [_vm._v("Si")]
                  ),
                  _vm._v(" "),
                  _c("v-spacer")
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/backend/historiales/ListadoAdminComponent.vue":
/*!**************************************************************************!*\
  !*** ./resources/js/views/backend/historiales/ListadoAdminComponent.vue ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ListadoAdminComponent_vue_vue_type_template_id_c919d1c0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ListadoAdminComponent.vue?vue&type=template&id=c919d1c0& */ "./resources/js/views/backend/historiales/ListadoAdminComponent.vue?vue&type=template&id=c919d1c0&");
/* harmony import */ var _ListadoAdminComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ListadoAdminComponent.vue?vue&type=script&lang=js& */ "./resources/js/views/backend/historiales/ListadoAdminComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ListadoAdminComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ListadoAdminComponent_vue_vue_type_template_id_c919d1c0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ListadoAdminComponent_vue_vue_type_template_id_c919d1c0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/backend/historiales/ListadoAdminComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/backend/historiales/ListadoAdminComponent.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/views/backend/historiales/ListadoAdminComponent.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListadoAdminComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListadoAdminComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/backend/historiales/ListadoAdminComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListadoAdminComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/backend/historiales/ListadoAdminComponent.vue?vue&type=template&id=c919d1c0&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/views/backend/historiales/ListadoAdminComponent.vue?vue&type=template&id=c919d1c0& ***!
  \*********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListadoAdminComponent_vue_vue_type_template_id_c919d1c0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListadoAdminComponent.vue?vue&type=template&id=c919d1c0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/backend/historiales/ListadoAdminComponent.vue?vue&type=template&id=c919d1c0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListadoAdminComponent_vue_vue_type_template_id_c919d1c0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListadoAdminComponent_vue_vue_type_template_id_c919d1c0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);