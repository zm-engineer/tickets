(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/NotaComponent.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/componentes/NotaComponent.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

moment__WEBPACK_IMPORTED_MODULE_0___default.a.locale('es');
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      menu: false,
      notaRecibida: null,
      dateFormatConfig: {
        dayOfWeekNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'],
        dayOfWeekNamesShort: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      }
    };
  },
  props: {
    nota: {
      type: String,
      "default": null
    },
    reservacion: {
      type: Object,
      "default": null
    },
    usuario: {
      type: Object,
      "default": null
    },
    precioNeto: {
      type: Number,
      "default": null
    },
    ticketId: {
      type: Number,
      "default": null
    }
  },
  computed: {
    notaSobreescrita: {
      get: function get() {
        return this.notaRecibida;
      },
      set: function set(newNote) {
        this.notaRecibida = newNote;
      }
    }
  },
  created: function created() {
    var _this = this;

    setTimeout(function () {
      _this.notaRecibida = _this.nota;
    }, 1000);
  },
  methods: {
    totalApagar: function totalApagar() {
      var usuario = this.reservacion.usuario;
      var neto, plus, precioPlus, comision, precioComision, plusTexto, comisionTexto;

      if (this.precioNeto % 1 != 0) {
        neto = parseFloat(this.precioNeto);
      } else {
        neto = parseInt(this.precioNeto);
      }

      plus = neto * usuario.plus;

      if (plus % 1 != 0) {
        plus = parseFloat(plus).toFixed(1);
        precioPlus = neto + parseFloat(plus);
      } else {
        plus = parseInt(plus);
        precioPlus = neto + parseInt(plus);
      }

      comision = precioPlus * usuario.comision;
      precioComision = precioPlus + comision;
      plusTexto = usuario.plus * 100;
      comisionTexto = usuario.comision * 100;
      plusTexto = plusTexto.toFixed(1) + '% plus';
      comisionTexto = comisionTexto.toFixed(1) + '% comisión';
      return neto + ' € + ' + plusTexto + ' + ' + comisionTexto + ' = ' + precioComision + ' €';
    },
    guardarNota: function guardarNota() {
      this.menu = false;
      this.$emit('guardarNota', this.ticketId, this.notaSobreescrita);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/NotaComponent.vue?vue&type=template&id=09d959dd&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/componentes/NotaComponent.vue?vue&type=template&id=09d959dd& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-row",
    { attrs: { justify: "space-around" } },
    [
      _c(
        "v-menu",
        {
          attrs: { "close-on-content-click": false, "nudge-width": 200 },
          scopedSlots: _vm._u([
            {
              key: "activator",
              fn: function(ref) {
                var on = ref.on
                var attrs = ref.attrs
                return [
                  _c(
                    "v-btn",
                    _vm._g(
                      _vm._b(
                        { attrs: { dark: "", icon: "" } },
                        "v-btn",
                        attrs,
                        false
                      ),
                      on
                    ),
                    [
                      _vm.nota
                        ? _c("v-icon", { attrs: { color: "black" } }, [
                            _vm._v("mdi-clipboard ")
                          ])
                        : _c("v-icon", { attrs: { color: "black" } }, [
                            _vm._v("mdi-clipboard-outline ")
                          ])
                    ],
                    1
                  )
                ]
              }
            }
          ]),
          model: {
            value: _vm.menu,
            callback: function($$v) {
              _vm.menu = $$v
            },
            expression: "menu"
          }
        },
        [
          _vm._v(" "),
          _c(
            "v-card",
            [
              _c(
                "v-list",
                [
                  _vm.reservacion
                    ? _c("v-list-item", [
                        _c("span", [
                          _c("strong", [_vm._v("Evento:")]),
                          _vm._v(" " + _vm._s(_vm.reservacion.evento.nombre))
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.reservacion
                    ? _c("v-list-item", [
                        _c("span", [
                          _c("strong", [_vm._v("Usuario:")]),
                          _vm._v(" " + _vm._s(_vm.reservacion.usuario.name))
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.reservacion && _vm.reservacion.conserje
                    ? _c("v-list-item", [
                        _c("span", [
                          _c("strong", [_vm._v("Conserje:")]),
                          _vm._v(" " + _vm._s(_vm.reservacion.conserje))
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.reservacion
                    ? _c("v-list-item", [
                        _c("span", [
                          _c("strong", [_vm._v("Fecha y hora:")]),
                          _vm._v(
                            " " +
                              _vm._s(
                                _vm._f("moment")(
                                  _vm.reservacion.created_at,
                                  "dddd, D MMMM YYYY HH:mm:ss"
                                )
                              )
                          )
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.reservacion
                    ? _c("v-list-item", [
                        _c("span", [
                          _c("strong", [_vm._v("Total a pagar:")]),
                          _vm._v(" " + _vm._s(_vm.totalApagar()))
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c(
                    "v-list-item",
                    [
                      _c(
                        "v-list-item-title",
                        [
                          _c("v-textarea", {
                            attrs: {
                              clearable: "",
                              "clear-icon": "mdi-close-circle",
                              rows: "2"
                            },
                            model: {
                              value: _vm.notaSobreescrita,
                              callback: function($$v) {
                                _vm.notaSobreescrita = $$v
                              },
                              expression: "notaSobreescrita"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  attrs: { text: "" },
                  on: {
                    click: function($event) {
                      _vm.menu = false
                    }
                  }
                },
                [_vm._v("\n        Cerrar\n      ")]
              ),
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  attrs: { color: "primary", text: "" },
                  on: {
                    click: function($event) {
                      return _vm.guardarNota()
                    }
                  }
                },
                [_vm._v("\n        Guardar\n      ")]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/componentes/NotaComponent.vue":
/*!****************************************************!*\
  !*** ./resources/js/componentes/NotaComponent.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _NotaComponent_vue_vue_type_template_id_09d959dd___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./NotaComponent.vue?vue&type=template&id=09d959dd& */ "./resources/js/componentes/NotaComponent.vue?vue&type=template&id=09d959dd&");
/* harmony import */ var _NotaComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./NotaComponent.vue?vue&type=script&lang=js& */ "./resources/js/componentes/NotaComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _NotaComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _NotaComponent_vue_vue_type_template_id_09d959dd___WEBPACK_IMPORTED_MODULE_0__["render"],
  _NotaComponent_vue_vue_type_template_id_09d959dd___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/componentes/NotaComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/componentes/NotaComponent.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/componentes/NotaComponent.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotaComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./NotaComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/NotaComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotaComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/componentes/NotaComponent.vue?vue&type=template&id=09d959dd&":
/*!***********************************************************************************!*\
  !*** ./resources/js/componentes/NotaComponent.vue?vue&type=template&id=09d959dd& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NotaComponent_vue_vue_type_template_id_09d959dd___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./NotaComponent.vue?vue&type=template&id=09d959dd& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/NotaComponent.vue?vue&type=template&id=09d959dd&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NotaComponent_vue_vue_type_template_id_09d959dd___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NotaComponent_vue_vue_type_template_id_09d959dd___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);