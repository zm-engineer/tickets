(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[9],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/backend/historiales/ListadoUsuarioComponent.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/backend/historiales/ListadoUsuarioComponent.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      search: '',
      headers: [{
        text: 'Evento',
        align: 'start',
        value: 'evento',
        sortable: false
      }, {
        text: 'Fecha y hora',
        value: 'fecha',
        sortable: false
      }, {
        text: 'Categoría',
        value: 'categoria',
        sortable: false
      }, {
        text: 'N° Tickets',
        value: 'numero',
        sortable: false
      }, {
        text: 'Sector',
        value: 'sector',
        sortable: false
      }, {
        text: 'Juntas',
        value: 'juntas',
        sortable: false
      }, {
        text: 'Formato',
        value: 'formato',
        sortable: false
      }, {
        text: 'Conserje',
        value: 'conserje',
        sortable: false
      }, {
        text: 'Fecha de reserva',
        value: 'created_at',
        sortable: false
      }, {
        text: 'Precio Final',
        value: 'precioUd',
        sortable: false
      }]
    };
  },
  props: {
    reservas: {
      type: Array,
      "default": []
    }
  },
  methods: {
    obtenerPrecioTotal: function obtenerPrecioTotal(reserva) {
      var dp = reserva.precioUd * reserva.plus;
      var pnp = parseFloat(reserva.precioUd) + parseFloat(dp);
      pnp = parseInt(pnp);
      var dc = pnp * reserva.comision;
      var totalUnitario = pnp + parseFloat(dc);
      var resultado = parseInt(totalUnitario);
      resultado = reserva.numero * resultado;
      resultado = resultado.toFixed(2);
      return parseInt(resultado);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/backend/historiales/ListadoUsuarioComponent.vue?vue&type=template&id=3ef569be&":
/*!*****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/backend/historiales/ListadoUsuarioComponent.vue?vue&type=template&id=3ef569be& ***!
  \*****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    [
      _c(
        "v-card-title",
        [
          _c("v-text-field", {
            attrs: {
              "append-icon": "mdi-magnify",
              label: "Buscar",
              "single-line": "",
              "hide-details": ""
            },
            model: {
              value: _vm.search,
              callback: function($$v) {
                _vm.search = $$v
              },
              expression: "search"
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c("v-data-table", {
        attrs: {
          headers: _vm.headers,
          items: _vm.reservas,
          search: _vm.search,
          "no-results-text": "No se encontraron datos",
          "no-data-text": "Sin resultados",
          "footer-props": {
            "items-per-page-options": [5, 10, 15, 20],
            "items-per-page-text": ""
          },
          "items-per-page": 5
        },
        scopedSlots: _vm._u([
          {
            key: "item.fecha",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("span", [
                  _vm._v(
                    _vm._s(_vm._f("moment")(item.fecha, "D MMMM YYYY HH:mm:ss"))
                  )
                ])
              ]
            }
          },
          {
            key: "item.created_at",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("span", [
                  _vm._v(
                    _vm._s(
                      _vm._f("moment")(
                        item.created_at,
                        "add",
                        "1 hours ",
                        "D MMMM YYYY HH:mm:ss"
                      )
                    )
                  )
                ])
              ]
            }
          },
          {
            key: "item.precioUd",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("span", [
                  _vm._v(_vm._s(_vm.obtenerPrecioTotal(item) + " €"))
                ])
              ]
            }
          },
          {
            key: "bootom",
            fn: function(ref) {
              var pagination = ref.pagination
              var options = ref.options
              var updateOptions = ref.updateOptions
              return [
                _c("v-data-footer", {
                  attrs: {
                    pagination: pagination,
                    options: options,
                    "items-per-page-text": "$vuetify.dataTable.itemsPerPageText"
                  },
                  on: { "update:options": updateOptions }
                })
              ]
            }
          }
        ])
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/backend/historiales/ListadoUsuarioComponent.vue":
/*!****************************************************************************!*\
  !*** ./resources/js/views/backend/historiales/ListadoUsuarioComponent.vue ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ListadoUsuarioComponent_vue_vue_type_template_id_3ef569be___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ListadoUsuarioComponent.vue?vue&type=template&id=3ef569be& */ "./resources/js/views/backend/historiales/ListadoUsuarioComponent.vue?vue&type=template&id=3ef569be&");
/* harmony import */ var _ListadoUsuarioComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ListadoUsuarioComponent.vue?vue&type=script&lang=js& */ "./resources/js/views/backend/historiales/ListadoUsuarioComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ListadoUsuarioComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ListadoUsuarioComponent_vue_vue_type_template_id_3ef569be___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ListadoUsuarioComponent_vue_vue_type_template_id_3ef569be___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/backend/historiales/ListadoUsuarioComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/backend/historiales/ListadoUsuarioComponent.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/views/backend/historiales/ListadoUsuarioComponent.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListadoUsuarioComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListadoUsuarioComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/backend/historiales/ListadoUsuarioComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ListadoUsuarioComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/backend/historiales/ListadoUsuarioComponent.vue?vue&type=template&id=3ef569be&":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/views/backend/historiales/ListadoUsuarioComponent.vue?vue&type=template&id=3ef569be& ***!
  \***********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListadoUsuarioComponent_vue_vue_type_template_id_3ef569be___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ListadoUsuarioComponent.vue?vue&type=template&id=3ef569be& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/backend/historiales/ListadoUsuarioComponent.vue?vue&type=template&id=3ef569be&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListadoUsuarioComponent_vue_vue_type_template_id_3ef569be___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ListadoUsuarioComponent_vue_vue_type_template_id_3ef569be___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);