(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/PlusComisionComponent.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/componentes/PlusComisionComponent.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      menu: false,
      userSeleccionado: null,
      precioNeto: null,
      plus: null,
      precioPlus: null,
      comision: null,
      precioComision: null,
      plusTexto: null,
      comisionTexto: null
    };
  },
  props: {
    ticket: {
      type: Object,
      "default": null
    },
    usuarios: {
      type: Array,
      "default": []
    }
  },
  created: function created() {},
  methods: {
    seleccionandoUser: function seleccionandoUser() {
      var _this = this;

      var usuario = this.usuarios.find(function (user) {
        return user.id === _this.userSeleccionado;
      });
      var neto, plus;
      this.precioNeto = this.ticket.precioUd;

      if (this.precioNeto % 1 != 0) {
        neto = parseFloat(this.precioNeto);
      } else {
        neto = parseInt(this.precioNeto);
      }

      this.plus = neto * usuario.plus;

      if (this.plus % 1 != 0) {
        this.plus = parseFloat(this.plus).toFixed(1);
        this.precioPlus = neto + parseFloat(this.plus);
      } else {
        this.plus = parseInt(this.plus);
        this.precioPlus = neto + parseInt(this.plus);
      }

      this.comision = this.precioPlus * usuario.comision;

      if (this.comision % 1 != 0) {
        this.comision = parseFloat(this.comision).toFixed(2);
        this.precioComision = this.precioPlus + parseFloat(this.comision);
      } else {
        this.comision = parseInt(this.comision);
        this.precioComision = this.precioPlus + parseInt(this.comision);
      }

      var resultadoConComisiones = this.precioComision; //---------CALCULAR REDONDEO---------

      var ultimaCifra, redondeoRealizado, resta;

      if (usuario.redondeo == 5) {
        ultimaCifra = resultadoConComisiones.toFixed(2).toString().slice(-4); // if(resultadoConComisiones % 1 == 0)
        //   ultimaCifra = resultadoConComisiones.toString().slice(-1)
        // else
        //   ultimaCifra = resultadoConComisiones.toString().slice(-4)

        if (Number(ultimaCifra) > 5) {
          resta = 10 - Number(ultimaCifra);
        } else {
          resta = 5 - Number(ultimaCifra);
        }

        redondeoRealizado = resultadoConComisiones + resta;
      } else if (usuario.redondeo == 1) {
        ultimaCifra = 0;
        if (resultadoConComisiones % 1 != 0) ultimaCifra = resultadoConComisiones.toFixed(2).toString().slice(-3);
        resta = resultadoConComisiones - Number(ultimaCifra);
        redondeoRealizado = resta + 1;
      } else {
        ultimaCifra = 0;
        if (resultadoConComisiones % 1 != 0) ultimaCifra = resultadoConComisiones.toFixed(2).toString().slice(-3);
        resta = resultadoConComisiones - Number(ultimaCifra);
        redondeoRealizado = resta;
      }

      this.precioComision = parseInt(redondeoRealizado.toFixed(2)); //-----------TEXTOS------------------

      this.plusTexto = usuario.plus * 100;
      this.comisionTexto = usuario.comision * 100;
      this.plusTexto = this.plusTexto.toFixed(1) + '% plus';
      this.comisionTexto = this.comisionTexto.toFixed(1) + '% comisión';
    },
    cerrar: function cerrar() {
      this.menu = false;
      this.userSeleccionado = null;
      this.precioNeto = null;
      this.plus = null;
      this.precioPlus = null;
      this.comision = null;
      this.precioComision = null;
      this.plusTexto = null;
      this.comisionTexto = null;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/PlusComisionComponent.vue?vue&type=template&id=15da98f4&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/componentes/PlusComisionComponent.vue?vue&type=template&id=15da98f4& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-row",
    { attrs: { justify: "space-around" } },
    [
      _c(
        "v-menu",
        {
          attrs: { "close-on-content-click": false, "nudge-width": 200 },
          scopedSlots: _vm._u([
            {
              key: "activator",
              fn: function(ref) {
                var on = ref.on
                var attrs = ref.attrs
                return [
                  _c(
                    "v-btn",
                    _vm._g(
                      _vm._b(
                        { attrs: { dark: "", icon: "" } },
                        "v-btn",
                        attrs,
                        false
                      ),
                      on
                    ),
                    [
                      _c("v-icon", { attrs: { color: "black" } }, [
                        _vm._v("mdi-currency-eur")
                      ])
                    ],
                    1
                  )
                ]
              }
            }
          ]),
          model: {
            value: _vm.menu,
            callback: function($$v) {
              _vm.menu = $$v
            },
            expression: "menu"
          }
        },
        [
          _vm._v(" "),
          _c(
            "v-card",
            [
              _c(
                "v-list",
                [
                  _c(
                    "v-list-item",
                    [
                      _c("v-list-item-title", [
                        _c("div", { staticClass: "mt-8" }, [
                          _c(
                            "span",
                            [
                              _c("v-autocomplete", {
                                attrs: {
                                  items: _vm.usuarios,
                                  dense: "",
                                  label: "Usuario",
                                  "item-text": "name",
                                  "item-value": "id",
                                  "no-data-text": "No se encontró"
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.seleccionandoUser()
                                  }
                                },
                                model: {
                                  value: _vm.userSeleccionado,
                                  callback: function($$v) {
                                    _vm.userSeleccionado = $$v
                                  },
                                  expression: "userSeleccionado"
                                }
                              })
                            ],
                            1
                          )
                        ])
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("v-list-item", [
                    _vm.comision
                      ? _c("div", [
                          _c("strong", [_vm._v("Precio neto:")]),
                          _vm._v(
                            " " + _vm._s(_vm.precioNeto) + " €\n            "
                          ),
                          _c("br"),
                          _vm._v(" "),
                          _c("strong", [
                            _vm._v("+" + _vm._s(_vm.plusTexto) + ":")
                          ]),
                          _vm._v(
                            " " +
                              _vm._s(_vm.precioNeto + " + " + _vm.plus) +
                              " = " +
                              _vm._s(_vm.precioPlus) +
                              " € \n            "
                          ),
                          _c("br"),
                          _vm._v(" "),
                          _c("strong", [
                            _vm._v("+" + _vm._s(_vm.comisionTexto) + ":")
                          ]),
                          _vm._v(
                            "  " +
                              _vm._s(_vm.precioPlus + " + " + _vm.comision) +
                              " = " +
                              _vm._s(_vm.precioComision) +
                              " €\n            "
                          ),
                          _c("br"),
                          _vm._v(" "),
                          _c("strong", [_vm._v("Precio Total:")]),
                          _vm._v(
                            "  " +
                              _vm._s(
                                _vm.precioComision * this.ticket.grupo.numero
                              ) +
                              " €\n          "
                          )
                        ])
                      : _vm._e()
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  attrs: { text: "" },
                  on: {
                    click: function($event) {
                      return _vm.cerrar()
                    }
                  }
                },
                [_vm._v("\n        Cerrar\n      ")]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/componentes/PlusComisionComponent.vue":
/*!************************************************************!*\
  !*** ./resources/js/componentes/PlusComisionComponent.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PlusComisionComponent_vue_vue_type_template_id_15da98f4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PlusComisionComponent.vue?vue&type=template&id=15da98f4& */ "./resources/js/componentes/PlusComisionComponent.vue?vue&type=template&id=15da98f4&");
/* harmony import */ var _PlusComisionComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PlusComisionComponent.vue?vue&type=script&lang=js& */ "./resources/js/componentes/PlusComisionComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PlusComisionComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PlusComisionComponent_vue_vue_type_template_id_15da98f4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PlusComisionComponent_vue_vue_type_template_id_15da98f4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/componentes/PlusComisionComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/componentes/PlusComisionComponent.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/componentes/PlusComisionComponent.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlusComisionComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./PlusComisionComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/PlusComisionComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PlusComisionComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/componentes/PlusComisionComponent.vue?vue&type=template&id=15da98f4&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/componentes/PlusComisionComponent.vue?vue&type=template&id=15da98f4& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PlusComisionComponent_vue_vue_type_template_id_15da98f4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./PlusComisionComponent.vue?vue&type=template&id=15da98f4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/componentes/PlusComisionComponent.vue?vue&type=template&id=15da98f4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PlusComisionComponent_vue_vue_type_template_id_15da98f4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PlusComisionComponent_vue_vue_type_template_id_15da98f4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);