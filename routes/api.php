<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'AuthController@login');
Route::post('/enviando-codigo', 'AuthController@codigo');
Route::post('/recuperar-contrasenia', 'AuthController@recuperar');
Route::post('/register', 'AuthController@register');
Route::middleware('auth:api')->post('/logout', 'AuthController@logout');
Route::middleware('auth:api')->post('/actualizar-ticket', 'TicketController@actualizar');
Route::middleware('auth:api')->post('/guardar-nota', 'TicketController@nota');
Route::middleware('auth:api')->post('/cambiar-visibilidad', 'EventoController@visibilidad');
Route::middleware('auth:api')->post('/actualizar-ticket-max', 'TicketController@cambiarTicketMax');

Route::middleware('auth:api')->get('/prueba', 'TicketController@prueba');

Route::middleware('auth:api')->apiResource('eventos', 'EventoController');
Route::middleware('auth:api')->apiResource('tickets', 'TicketController');
Route::middleware('auth:api')->apiResource('/reservacion', 'ReservacionController');
Route::middleware('auth:api')->apiResource('/usuarios', 'UsuarioController');
Route::middleware('auth:api')->apiResource('/categorias', 'CategoriaController');
Route::middleware('auth:api')->apiResource('/grupos', 'GrupoController');
Route::middleware('auth:api')->apiResource('/preguntas', 'PreguntaController');
Route::apiResource('/configuracion', 'ConfiguracionController');
Route::middleware('auth:api')->apiResource('/mapas', 'MapaController');
Route::middleware('auth:api')->apiResource('/tipos', 'TiposController');


